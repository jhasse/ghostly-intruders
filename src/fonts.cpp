#include "fonts.hpp"

namespace fonts {

std::shared_ptr<jngl::Font> veryBig() {
	static const auto f = std::make_shared<jngl::Font>(jngl::getFont(), 250);
	return f;
}

}
