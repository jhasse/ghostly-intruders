#include "Menu.hpp"

#include "LevelEditor.hpp"
#include "engine/Fade.hpp"
#include "engine/Paths.hpp"
#include "OptionsMenu.hpp"
#include "Game.hpp"

const std::string Menu::backgroundMusic = "../data/sfx/mausoleum.ogg";

Menu::Menu() : logo(GetPaths().Graphics() + "logo") {
	auto buttonBox = std::make_shared<ButtonBox>(0, 300);
	buttonBox->Add("Play",
	               []() { jngl::setWork(std::make_shared<Fade>(std::make_shared<Game>())); });
	buttonBox->Add("Options", []() {
		jngl::setWork(std::make_shared<Fade>(std::make_shared<OptionsMenu>()));
	});
	buttonBox->Add("Quit", jngl::quit);
	AddWidget(buttonBox);
	logo.setCenter(0, -500);

	jngl::play(backgroundMusic);
}

Menu::~Menu() {
	jngl::stop(backgroundMusic);
}

void Menu::step() {
	StepWidgets();
}

void Menu::draw() const {
	jngl::setBackgroundColor({50, 50, 50});
	logo.draw();
	DrawWidgets();
}
