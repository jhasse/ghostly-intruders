#include "CollisionLine.hpp"

#include <jngl.hpp>

CollisionLine::CollisionLine(Vector2d start, Vector2d end)
: start_(std::move(start)), end_(std::move(end)) {
	position = Vector2d(0, 0);
	setShape(new Line(*this, start_, end_));
}

void CollisionLine::step() {
	collision_ = false;
}

void CollisionLine::draw() const {
#ifndef NDEBUG
	jngl::setColor(255, 70, 70, 255);
	if (collision_) {
		jngl::setColor(255, 0, 0);
	}
	jngl::drawLine(start_.X(), start_.Y(), end_.X(), end_.Y());
#endif
}

Vector2d CollisionLine::GetStart() const {
	return start_;
}

Vector2d CollisionLine::GetEnd() const {
	return end_;
}

void CollisionLine::SetCollision(bool) {
	collision_ = true;
}

ImpassableWall::ImpassableWall(Vector2d start, Vector2d end)
: CollisionLine(std::move(start), std::move(end)) {
}
