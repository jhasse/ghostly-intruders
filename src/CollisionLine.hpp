#pragma once

#include "engine/GameObject.hpp"

class CollisionLine : public GameObject {
public:
	CollisionLine(Vector2d start, Vector2d end);
	void step();
	void draw() const;
	Vector2d GetStart() const;
	Vector2d GetEnd() const;
	void SetCollision(bool);
private:
	Vector2d start_, end_;
	bool collision_;
};

// Auch Geister können nicht durch diese Wand
class ImpassableWall : public CollisionLine {
public:
	ImpassableWall(Vector2d start, Vector2d end);
};
