#pragma once

#include "Player.hpp"
#include "engine/Animation.hpp"

class DeadGhost : public Player {
public:
	DeadGhost(Vector2d position, int numberOfOrbs, std::unique_ptr<Control>, int playerNr);
	void step() override;
	void draw() const override;
	void collectOrb();

private:
	Animation animation;
	int orbsLeft;
	int fadeIn = 0;
};
