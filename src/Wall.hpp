#pragma once

#include "engine/GameObject.hpp"

#include <jngl/sprite.hpp>

class Wall : public GameObject {
public:
	Wall(Vector2d position, std::string filename, std::vector<std::pair<double, double>> doors);
	void step() override;
	void draw() const override;

private:
	jngl::Sprite sprite;
};
