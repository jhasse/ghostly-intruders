#include "ReloadGame.hpp"

#include "Game.hpp"

void ReloadGame::step() {
	jngl::setWork(std::make_shared<Game>());
}

void ReloadGame::draw() const {
}
