#pragma once

#include "../Level.hpp"
#include "../engine/coroutines.hpp"

class Game;

class Level1 : public Level {
public:
	Level1(void* game);
	void step(push_type&) override;
};
