#include "Level1.hpp"

#include "../constants.hpp"

#include <fstream>

Level1::Level1(void* game) : Level(*reinterpret_cast<Game*>(game)) {
}

void Level1::step(push_type& yield) {
	yield();

	addWell(-1670, -600);
	addWell(1670, 600);

	addPlayer(-800, 0); // Vater
	addPlayer(1850, -600);
	addPlayer(-300, 0); // Mutter
	addPlayer(-1850, 300);

	addImpassableWall(-1920, -1080,  1920, -1080);
	addImpassableWall( 1920, -1080,  1920,  1080);
	addImpassableWall( 1920,  1080, -1920,  1080);
	addImpassableWall(-1920,  1080, -1920, -1080);

	const int tuerenUnten = 190;
	const int tuerenHoehe = 380;
	const int vertikalesWandOffset = 55;

	// Oben links nach oben rechts:
	addLine(-1420 + wallThickness, -1048 + +wallThickness + vertikalesWandOffset, 1422 - wallThickness,
	        -1048 + wallThickness + vertikalesWandOffset);
	// Unten links nach unten rechts:
	addLine(-1420 + wallThickness, 1048 - vertikalesWandOffset - wallThickness, 1422 - wallThickness,
	        1048 - vertikalesWandOffset - wallThickness);

	for (int i : { 0, wallThickness }) {
		// Links:
		addLine(-1420 + i, -1048, -1420 + i, tuerenUnten - tuerenHoehe);
		addLine(-1420 + i, tuerenUnten, -1420 + i, 1048);


		// Zweite Wand oben:
		addLine(-824 + i, -1048 + wallThickness, -824 + i, tuerenUnten - tuerenHoehe);
		// Dritte Wand oben:
		addLine(-204 + i, -1048 + wallThickness, -204 + i, tuerenUnten - tuerenHoehe);
		// Vierte Wand oben:
		addLine(178 + i, -1048 + wallThickness, 178 + i, tuerenUnten - tuerenHoehe);

		// Zweite Wand unten:
		addLine(-524 - i, tuerenUnten, -524 - i, 900);
		// Dritte Wand unten:
		addLine(-174 + i, tuerenUnten, -174 + i, 900);

		// Rechts:
		addLine(1422 - i, -1048, 1422 - i, tuerenUnten - tuerenHoehe);
		addLine(1422 - i, tuerenUnten, 1422 - i, 1048);
	}

	const int doorWidth = 210;
	addWall(1, -159, "wand_m", {
		{218, 218 + doorWidth},
		{956, 956 + doorWidth},
		{1252, 1604},
		{2540, 2540 + doorWidth},
	});
	addWall(-973, 204, "wand1", {{428, 428 + doorWidth}});
	addWall(621, 194, "wand2", {{126, 126 + doorWidth}});

	addKid(-1150, -600); // oben links

	addKid( -950,  600); // unten links

	addKid(  300, -500); // oben rechts
	addKid( 1200, -600); // oben rechts

	addKid(  500,  600); // unten rechts
	addKid(  800,  600); // unten rechts
	addKid( 1100,  600); // unten rechts

	// std::ifstream ifs(getTxtPath());
	// if (ifs) {
	// 	iarchive archive(ifs);
	// 	std::vector<BorderArea> borderAreas;
	// 	archive >> borderAreas;
	// 	for (const auto& borderArea : borderAreas) {
	// 		// TODO: Einfach nur CollisionLines erstellen
	// 	}
	// } else {
	// 	throw std::runtime_error("Couldn't read " + getTxtPath() + "!");
	// }

	while (true) { yield(); }
}

extern "C" {
	void* createLevel(void* game) {
		return static_cast<Level*>(new Level1(game));
	}
}
