#include "Beam.hpp"

#include "engine/Paths.hpp"

#include <jngl.hpp>

Beam::Beam(const Vector2d start)
: sprite(GetPaths().Graphics() + "strahl"), length(sprite.getWidth() - 20) {
	position = start;
	sprite.setCenter(length / 2, 0);

	// Das Ende der Linie wird durch Parent::step jeden Frame aktualisiert.
	setShape(new Line(*this, Vector2d(12, -2), Vector2d(0, 0)));
}

void Beam::step() {
}

void Beam::draw() const {
	const auto& line = dynamic_cast<const Line&>(GetShape());
	jngl::pushMatrix();
	jngl::translate(position + line.getRelStart());
	jngl::rotate(direction.toDegree());
	jngl::pushSpriteAlpha(160);
	sprite.drawClipped({ 0, 0 }, { line.getLength() / length, 1 });
	jngl::popSpriteAlpha();
	jngl::popMatrix();
}

void Beam::setPositionAndDirection(const Vector2d position, const Vector2d direction) {
	this->position = position;
	if (direction.LengthSq() > 0.1) {
		this->direction = direction.Normalize();
		dynamic_cast<Line&>(getShape()).setRelEnd(this->direction * length);
	}
}

void Beam::collisionAt(const Vector2d collision) {
	auto& line = dynamic_cast<Line&>(getShape());
	if ((collision - line.getAbsStart()).LengthSq() <
	    (line.getAbsEnd() - line.getAbsStart()).LengthSq()) {
		line.setRelEnd(collision - position);
	}
}
