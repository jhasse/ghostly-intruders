#pragma once

#include "engine/dll.hpp"

#include <string>
#include <functional>

class SharedObject {
public:
	SharedObject(const std::string& folder, const std::string& name);
	SharedObject(const SharedObject&) = delete;
	SharedObject(SharedObject&&) = default;
	DllExport ~SharedObject();
	std::function<void*(void*)> loadFunction(const std::string& name);

	// Level1.so -> gibt Level1 zurück
	std::string getName() const;

private:
	std::string name;
};
