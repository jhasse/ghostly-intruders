#pragma once

#include "Control.hpp"

class Keyboard : public Control {
public:
	Vector2d getAcceleration() const override;
	Vector2d getDirection() const override;
	bool getShoot() const override;
};
