#include "Orb.hpp"

#include "engine/Paths.hpp"

#include <cmath>

Orb::Orb(const Vector2d position) : sprite(GetPaths().Graphics() + "orb") {
	this->position = position,
	sprite.setCenter(position);
	overwriteZIndex(99999);
	setShape(new Circle(*this, 20));
}

void Orb::step() {
	++frameCounter;
}

void Orb::draw() const {
	jngl::setColor(0, 0, 0, frameCounter >= 220 ? 80 : float(80 * frameCounter) / 220.0f);
	jngl::drawEllipse(position.x, position.y + 40, 24, 10);
	jngl::pushMatrix();
	jngl::translate(0, 8 * std::sin(float(frameCounter) / 10.0f));
	jngl::pushSpriteAlpha(frameCounter >= 220 ? 220 : frameCounter);
	sprite.draw();
	jngl::popMatrix();
	jngl::popSpriteAlpha();
}
