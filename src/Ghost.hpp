#pragma once

#include "engine/Animation.hpp"
#include "Player.hpp"

#include <string>
#include <optional>

class EnemyShot;
class Parent;

class Ghost : public Player {
public:
	Ghost(Vector2d position, int playerNr, std::unique_ptr<Control> = nullptr);
	virtual void draw() const;
	virtual void step();
	virtual double GetHP() const;

	void attack();

	bool isUnderAttack() const;

private:
	const static int MAX_HP;
	int shotdim_;
	mutable Vector2d cameraPosition_;
	mutable Vector2d mouse_;

	Animation animation;
	Animation animationShock;

	int hp;
	unsigned int numberOfAttackers = 0;

	/// Brauchen wir, da während der Kollisionserkennung isUnderAttack aufgerufen wird.
	unsigned int numberOfAttackersFromPreviousFrame = 0;

	const static int INVISIBLE_DURATION;
	const static int INVISIBLE_COOLDOWN;
	int invisibleCountdown = 0;
};
