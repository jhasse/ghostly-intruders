#include "Wall.hpp"

#include "CollisionLine.hpp"
#include "constants.hpp"
#include "engine/Paths.hpp"
#include "Game.hpp"

Wall::Wall(Vector2d position, std::string filename, std::vector<std::pair<double, double>> doors)
: sprite(GetPaths().Graphics() + filename) {
	for (size_t i = 0; i <= doors.size(); ++i) {
		for (int yOffset : { 0, wallThickness }) {
			Vector2d start = position + Vector2d(-sprite.getWidth() / 2,
			                                     -wallThickness / 2 + yOffset);
			Vector2d end = position + Vector2d(sprite.getWidth() / 2,
			                                   -wallThickness / 2 + yOffset);
			if (i < doors.size()) {
				end.x = start.x + doors[i].first;
			}
			if (i > 0) {
				start.x += doors[i - 1].second;
			}
			GetGame().add<CollisionLine>(start, end);
		}
	}
	sprite.setCenter(position);
	this->position = std::move(position);
}

void Wall::step() {
}

void Wall::draw() const {
	sprite.draw();
}
