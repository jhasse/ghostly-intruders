#include "Well.hpp"

#include "engine/Paths.hpp"
#include "Player.hpp"
#include "Game.hpp"

Well::Well(Vector2d position)
: CollisionCircle(position, 100), sprite(GetPaths().Graphics() + "brunn") {
	sprite.setCenter(position + Vector2d(5, 25));
	this->position = std::move(position);
	setShape(new Circle(*this, radius));
}

void Well::step() {
	for (auto& it : recentlyReceived) {
		if (it.second > 0) { --(it.second); }
	}
}

void Well::draw() const {
	sprite.draw();
#ifndef NDEBUG
	jngl::setColor(255, 0, 0, 100);
	jngl::drawEllipse(position.x, position.y, radius, radius);
#endif
}

void Well::teleport(Player& player) {
	if (recentlyReceived[&player] > 0) {
		recentlyReceived[&player] = 60;
		return;
	}
	auto wells = GetGame().GetAll<Well>();
	if (wells.size() != 2) {
		throw std::runtime_error("There need to be exactly two wells!");
	}
	Well* otherWell = wells.front();
	if (otherWell == this) {
		otherWell = wells.back();
	}
	otherWell->receivePlayer(player);
}

void Well::receivePlayer(Player& player) {
	recentlyReceived[&player] = 60;
	player.teleportTo(position);
}
