#include "collisionobjects.hpp"

#include "engine/Screen.hpp"

#include <jngl.hpp>

void CollisionRectangle::SetCollision(bool) {
	collision_ = true;
}

CollisionRectangle::CollisionRectangle(const Vector2d position, int size)
: size_(size), collision_(false) {
	this->position = position;
	setShape(new Quadrat(*this, size));
}

void CollisionRectangle::draw() const {
	jngl::setColor(255, 255, 255);
	if (collision_) {
		jngl::setColor(255, 0, 0);
	}
	const Vector2d topleft = position - Vector2d(size_, size_) / 2;
	const Vector2d topright = topleft + Vector2d(size_, 0);
	const Vector2d bottomleft = topleft + Vector2d(0, size_);
	const Vector2d bottomright = topleft + Vector2d(size_, size_);
	GetScreen().drawLine(topleft, topright);
	GetScreen().drawLine(topright, bottomright);
	GetScreen().drawLine(bottomright, bottomleft);
	GetScreen().drawLine(bottomleft, topleft);
}

void CollisionRectangle::step() {
	collision_ = false;
}

void CollisionCircle::SetCollision(bool) {
	collision_ = true;
}

CollisionCircle::CollisionCircle(const Vector2d position, const int radius)
: radius(radius), collision_(false) {
	this->position = position;
	setShape(new Circle(*this, radius));
}

void CollisionCircle::draw() const {
	jngl::setColor(255, 255, 255);
	if (collision_) {
		jngl::setColor(255, 0, 0);
	}
}

void CollisionCircle::step() {
	collision_ = false;
}
