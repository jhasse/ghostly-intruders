#include "OptionsMenu.hpp"

#include "Menu.hpp"
#include "engine/Fade.hpp"

#include <boost/bind.hpp>

OptionsMenu::OptionsMenu() {
	buttons.emplace_back("Apply", [](){});
	buttons[0].CenterAt(600, 400);
	buttons.emplace_back(
	    "Back", []() { jngl::setWork(std::make_shared<Fade>(std::make_shared<Menu>())); });
	buttons[1].CenterAt(-600, 400);
}

void OptionsMenu::step() {
	for (auto& button : buttons) {
		button.step();
	}
}

void OptionsMenu::draw() const {
	jngl::setFontColor(255, 255, 255);
	jngl::print("Resolution:", -300, -500);
	for (const auto& button : buttons) {
		button.draw();
	}
}
