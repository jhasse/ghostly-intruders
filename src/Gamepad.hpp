#pragma once

#include "Control.hpp"

#include <jngl/Controller.hpp>

class Gamepad : public Control {
public:
	Gamepad(std::shared_ptr<jngl::Controller>);
	Vector2d getAcceleration() const override;
	Vector2d getDirection() const override;
	bool getShoot() const override;

private:
	std::shared_ptr<jngl::Controller> controller;
};
