#pragma once

#include "engine/GameObject.hpp"
#include "engine/Animation.hpp"

#include <jngl/sprite.hpp>

class Kid : public GameObject {
public:
	Kid(Vector2d);
	void step() override;
	void draw() const override;

	void attack();
	bool isAwake() const;

private:
	int hp;
	int numberOfAttackingGhosts = 0;
	jngl::Sprite spriteSleeping;
	jngl::Sprite spriteAwake;

	/// 0 - schlafend
	/// 1 - Warndreieck
	/// 2 - weinend
	std::array<Animation, 3> animations;
	Animation* activeAnimation;
};
