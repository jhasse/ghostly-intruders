#pragma once

#include "Button.hpp"

#include <vector>
#include <memory>

class ButtonBox : public Widget {
public:
	ButtonBox(int xCenter, int yCenter);
	void Add(const std::string& text, std::function<void()>, char shortcut = ' ');
	void draw() const override;
	void step() override;
	int GetMouseover() const; // Returns over which button the mouse is over. If there's none -1.
	virtual void onAdd(Work&) override;
private:
	std::vector<std::shared_ptr<Button> > buttons_;
	const int xCenter_, yCenter_;
	const static int spacing_;
};
