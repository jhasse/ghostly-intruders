#include "ButtonBox.hpp"

#include "../engine/Work.hpp"

const int ButtonBox::spacing_ = 15;

ButtonBox::ButtonBox(const int xCenter, const int yCenter)
	: xCenter_(xCenter), yCenter_(yCenter)
{
	setSensitive(false);
}

void ButtonBox::Add(const std::string& text, std::function<void()> function, const char shortcut) {
	buttons_.push_back(std::shared_ptr<Button>(new Button(text, function, shortcut)));

	std::vector<std::shared_ptr<Button> >::iterator end = buttons_.end();
	int yPosButton = yCenter_ - (int(buttons_.size()) * (buttons_[0]->GetHeight() + spacing_) - spacing_) / 2 + buttons_[0]->GetHeight() / 2;
	for(std::vector<std::shared_ptr<Button> >::iterator it = buttons_.begin(); it != end; ++it)
	{
		(*it)->CenterAt(xCenter_, yPosButton);
		yPosButton += buttons_[0]->GetHeight() + spacing_;
	}
}

void ButtonBox::draw() const {
}

void ButtonBox::step() {
}

int ButtonBox::GetMouseover() const
{
	for(std::size_t i = 0; i < buttons_.size(); ++i)
	{
		if(buttons_[i]->Mouseover())
		{
			return static_cast<int>(i);
		}
	}
	return -1;
}

void ButtonBox::onAdd(Work& work) {
	auto end = buttons_.end();
	for (auto it = buttons_.begin(); it != end; ++it) {
		work.AddWidget(*it);
	}
}
