#include "Button.hpp"

#include "../engine/Paths.hpp"
#include "../engine/Screen.hpp"

#include <jngl/all.hpp>

const int Button::fontSize_ = 60;

Button::Button(const std::string& text)
: text_(text), xPos_(0), yPos_(0), mouseoverAlpha_(0), clicked_(false) {
	SetSprites("button", "button_over", "button_clicked");
}

Button::Button(const std::string& text, std::function<void()> callback, char shortcut)
: text_(text), xPos_(0), yPos_(0), mouseoverAlpha_(0), shortcut_(shortcut), callback_(callback),
  clicked_(false) {
	SetSprites("button", "button_over", "button_clicked");
}

void Button::SetText(const std::string& text) {
	text_ = text;
}

void Button::SetSprites(const std::string& normal, const std::string& mouseOver,
                        const std::string& clicked) {
	texture_ = normal;
	textureMouseOver_ = mouseOver;
	textureClicked_ = clicked;
	width_ = jngl::getWidth(GetPaths().Graphics() + texture_);
	height_ = jngl::getHeight(GetPaths().Graphics() + texture_);
}

void Button::CenterAt(const int xCenter, const int yCenter) {
	xPos_ = xCenter;
	yPos_ = yCenter;
	jngl::setFontSize(fontSize_);
}

void Button::draw() const {
	int alpha = mouseoverAlpha_;
	if (clicked_) {
		alpha -= 100;
	}
	jngl::pushMatrix();
	jngl::translate(xPos_, yPos_);
	jngl::scale(1.0f + (alpha / 6000.0f));
	GetScreen().drawCentered(texture_, 0, 0);
	if (focus_) {
		GetScreen().drawCentered(textureMouseOver_, 0, 0);
	}
	jngl::setSpriteColor(255, 255, 255, alpha);
	GetScreen().drawCentered(textureMouseOver_, 0, 0);
	jngl::setSpriteColor(255, 255, 255, 255);
	if (clicked_) {
		GetScreen().drawCentered(textureClicked_, 0, 0);
	}
	jngl::setFontColor(0, 0, 0);
	jngl::setFontSize(fontSize_);
	if (!clicked_) {
		GetScreen().printCentered(text_, 0, 0);
	} else {
		GetScreen().printCentered(text_, 5, 5);
	}
	jngl::popMatrix();
}

void Button::step() {
	if (!jngl::mouseDown()) {
		clicked_ = false;
	}
	const int alphaSpeed = 20;
	if (focus_) {
		if (jngl::keyPressed(jngl::key::Space) || jngl::keyPressed(jngl::key::Return)) {
			clicked_ = true;
			callback_();
		}
	}
	if (Mouseover()) {
		if (mouseoverAlpha_ < 255) {
			mouseoverAlpha_ += alphaSpeed;
		}
		if (jngl::mousePressed()) {
			clicked_ = true;
			jngl::play(GetPaths().data() + "sfx/button.ogg");
			callback_();
		}
	} else if (mouseoverAlpha_ > 0) {
		mouseoverAlpha_ -= alphaSpeed;
	}
	if (mouseoverAlpha_ > 255) {
		mouseoverAlpha_ = 255;
	}
	if (mouseoverAlpha_ < 0) {
		mouseoverAlpha_ = 0;
	}
}

bool Button::Mouseover() const {
	return (xPos_ - width_ / 2 <= GetScreen().getMouseX() &&
	        GetScreen().getMouseX() < (xPos_ + width_ / 2) &&
	        yPos_ - height_ / 2 <= GetScreen().getMouseY() &&
	        GetScreen().getMouseY() < (yPos_ + height_ / 2));
}

int Button::GetX() const {
	return xPos_;
}

int Button::GetY() const {
	return yPos_;
}

int Button::GetHeight() {
	return height_;
}

int Button::GetWidth() {
	return width_;
}

char Button::ShortCutClicked() {
	return jngl::keyDown(shortcut_);
}

void Button::Connect(std::function<void()> callback) {
	callback_ = callback;
}
