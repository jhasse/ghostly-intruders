#pragma once

#include "engine/Vector2d.hpp"
#include "leveleditor/VertexGrabber.hpp"

#include <vector>
#include <functional>

class BorderArea {
public:
	BorderArea() = default;
	BorderArea(std::vector<Vector2d> vertices, std::vector<std::vector<Vector2d>> holes);

	/// Gibt true zurück, wenn sich etwas geändert hat
	bool step();

	void draw() const;
	DllExport void
	create(const std::function<void(std::vector<Vector2d> vertices,
	                                std::vector<std::vector<Vector2d>> holes)>&) const;

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive& ar, const unsigned int) {
		ar & vertices;
		ar & holes;
	}

	std::vector<VertexGrabber> vertices;
	std::vector<std::vector<VertexGrabber>> holes;
};

BOOST_CLASS_EXPORT_KEY(BorderArea)
