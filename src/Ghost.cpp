#include "Ghost.hpp"

#include "Game.hpp"
#include "engine/Vector2d.hpp"
#include "engine/Screen.hpp"
#include "engine/Paths.hpp"
#include "Control.hpp"
#include "DeadGhost.hpp"
#include "Orb.hpp"

const int Ghost::MAX_HP = 320;
const int Ghost::INVISIBLE_DURATION = 300;
const int Ghost::INVISIBLE_COOLDOWN = 360;

Ghost::Ghost(const Vector2d position, const int playerNr, std::unique_ptr<Control> control)
: Player(playerNr, std::move(control)),
  animation("ghost" + std::to_string((playerNr / 2 % 2) + 1) + "_cycle1"),
  animationShock("g" + std::to_string((playerNr / 2 % 2) + 1) + "_shock"), hp(MAX_HP) {
	this->position = position;
	shotdim_ = 0;
	life_ = 100;
	setShape(new Circle(*this, 45));
	overwriteZIndex(99999);
}

template <class T> constexpr const T& clamp(const T& v, const T& lo, const T& hi) {
	return clamp(v, lo, hi, std::less<>());
}

void Ghost::draw() const {
	jngl::pushMatrix();
	jngl::translate(position.X(), position.Y() - 10); // Etwas höher zeichnen wegen des Schattens
	jngl::scale(animationScale);

	const int numberOfFadeFrames = 20;
	const int countdownMax = INVISIBLE_COOLDOWN + INVISIBLE_DURATION - numberOfFadeFrames;
	const int countdownMin = INVISIBLE_COOLDOWN - numberOfFadeFrames;
	float fadeScale;
	if (invisibleCountdown > countdownMax) {
		fadeScale = float(invisibleCountdown - countdownMax) / numberOfFadeFrames;
	} else {
		fadeScale = clamp(float(countdownMin - invisibleCountdown) / numberOfFadeFrames,
		                  0.0f, 1.0f);
	}
	jngl::setColor(0, 0, 0, 80 * animationScale * fadeScale);
	jngl::drawEllipse(0, 0 + 74, 41, 16);

	if (spriteDirection == Direction::LEFT) {
		jngl::scale(-1, 1); // spiegeln
	}
	jngl::translate(-16, 0); // Versatz für den Schwanz

	jngl::pushSpriteAlpha(225 * animationScale * fadeScale);
	if (numberOfAttackers > 0) {
		// Immer schneller anfangen zu blinken:
		const auto random = rand() % hp;
		jngl::pushSpriteAlpha(random < 6 ? 40 : (random < 36 ? 80 : (random < 180 ? 120 : 255)));
		animationShock.draw();
		jngl::popSpriteAlpha();
	} else {
		animation.draw();
	}
	jngl::popSpriteAlpha();
	jngl::popMatrix();

	auto tmp = Vector2d(GetScreen().getMouseX(), GetScreen().getMouseY());
	if (tmp != mouse_) {
		mouse_ = tmp;
	}
}

void Ghost::attack() {
	++numberOfAttackers;
}

bool Ghost::isUnderAttack() const {
	return numberOfAttackersFromPreviousFrame > 0;
}

void Ghost::step() {
	animation.step();
	animationShock.step();
	if (life_ <= 0) {
		GetGame().PlayerDies();
		return;
	}

	stepMovement(invisibleCountdown == 0 ? 0.3 : 0.5);

	if (jngl::keyPressed('c')) {
		std::ostringstream sstream;
		sstream << "bash -c \"echo '{ " << (cameraPosition_ + mouse_).X() << ", "
		        << (cameraPosition_ + mouse_).Y() << " },' | xclip -selection clipboard\"";
		if (system(sstream.str().c_str()) != 0) {
			std::cout << sstream.str() << std::endl;
		}
	}

	const static std::string sound = GetPaths().data() + "sfx/light.ogg";
	if (numberOfAttackers > 0) {
		if (!jngl::isPlaying(sound)) {
			jngl::play(sound);
		}
		speed *= 0.9; // verlangsamen
		hp -= numberOfAttackers;
	}
	if (hp <= 0) {
		jngl::play(GetPaths().data() + "sfx/dead.ogg");
		const auto animation = GetGame().add<Animation>("explo", 1, false);
		animation->setPosition(position);
		animation->overwriteZIndex(99999);
		const int NUMBER_OF_ORBS = 3;
		for (int i = 0; i < NUMBER_OF_ORBS; ++i) {
			GetGame().add<Orb>(Vector2d(1800 - rand() % 3600, 900 - rand() % 1800));
		}
		GetGame().add<DeadGhost>(position, NUMBER_OF_ORBS, std::move(control), playerNr);
		GetGame().Remove(this);
		return; // control has moved to DeadGhost
	}

	if (invisibleCountdown > 0) {
		--invisibleCountdown;
		if (invisibleCountdown == INVISIBLE_COOLDOWN) {
			jngl::play(GetPaths().data() + "sfx/sichtbar.ogg");
		}
	}
	if (invisibleCountdown == 0 && control->getShoot()) {
		invisibleCountdown = INVISIBLE_DURATION + INVISIBLE_COOLDOWN;
		jngl::play(GetPaths().data() + "sfx/unsichtbar.ogg");
	}

	numberOfAttackersFromPreviousFrame = numberOfAttackers;
	numberOfAttackers = 0; // Wird von der Kollisionsbehandlung wieder gesetzt
}

double Ghost::GetHP() const {
	if (life_ < 0) {
		return 0;
	}
	return life_;
}
