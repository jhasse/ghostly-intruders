#pragma once

#include "engine/GameObject.hpp"

#include <vector>

class Vector2d;
class Control;
class pull_type;

/// Entweder ein Elternteil oder ein Geist
class DllExport Player : public virtual MoveableObject {
public:
	Player(size_t playerNr, std::unique_ptr<Control> = nullptr);
	Player(const Player&) = delete;
	virtual ~Player();
	void draw() const override;
	void Accelerate(const Vector2d&) override;
	virtual double GetRadius() const;
	void teleportTo(Vector2d position);

	/// Einen Richtungsvektor angeben aus dessen Richtung unendlicher Widerstand herscht
	void blockMovementFrom(Vector2d);

protected:
	void stepMovement(double movementSpeed);

	std::vector<Vector2d> blockedDirections;
	double life_;

	std::unique_ptr<Control> control;
	enum class Direction {
		LEFT,
		RIGHT,
	};
	Direction spriteDirection = Direction::RIGHT;

	std::unique_ptr<pull_type> animation;
	double animationScale = 1.0;

	const int playerNr;
};
