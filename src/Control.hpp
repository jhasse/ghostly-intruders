#pragma once

#include "engine/Vector2d.hpp"

class Control {
public:
	virtual ~Control() = default;
	virtual Vector2d getAcceleration() const = 0;
	virtual Vector2d getDirection() const = 0;
	virtual bool getShoot() const = 0;
};
