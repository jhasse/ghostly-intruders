#include "DeadGhost.hpp"

#include "Control.hpp"
#include "Ghost.hpp"
#include "Game.hpp"
#include "engine/Paths.hpp"

#include <jngl.hpp>

DeadGhost::DeadGhost(const Vector2d position, const int numberOfOrbs,
                     std::unique_ptr<Control> control, const int playerNr)
: Player(playerNr, std::move(control)), animation("dead_ghost"), orbsLeft(numberOfOrbs) {
	this->position = position;
	overwriteZIndex(99999);
	setShape(new Circle(*this, 40));
}

void DeadGhost::collectOrb() {
	--orbsLeft;
	if (orbsLeft == 0) {
		jngl::play(GetPaths().data() + "sfx/respawn.ogg");
	} else {
		jngl::play(GetPaths().data() + "sfx/orb.ogg");
	}
}

void DeadGhost::step() {
	if (fadeIn < 200) { ++fadeIn; }
	stepMovement(0.3);
	animation.step();
	if (orbsLeft == 0) {
		GetGame().add<Ghost>(position, playerNr, std::move(control));
		GetGame().Remove(this);
	}
}

void DeadGhost::draw() const {
	Player::draw();
	jngl::pushSpriteAlpha(fadeIn);
	jngl::pushMatrix();
	jngl::translate(position);
	animation.draw();
	jngl::popSpriteAlpha();
	jngl::popMatrix();
}
