#include "SharedObject.hpp"

#include "levels/Level1.hpp"

#include <boost/filesystem.hpp>
#include <dlfcn.h>
#include <stdexcept>

#ifdef _WIN32
const std::string DLL_SUFFIX = ".dll";
#else
const std::string DLL_SUFFIX = ".so";
#endif

SharedObject::SharedObject(const std::string& folder, const std::string& name) : name(name) {
}

SharedObject::~SharedObject() {
}

std::function<void*(void*)> SharedObject::loadFunction(const std::string& name) {
	if (this->name == "Level1") {
		return [](void* game) -> void* {
			return new Level1(game);
		};
	}
	throw std::runtime_error("Don't know how to load " + name);
}

std::string SharedObject::getName() const {
	return name;
}
