#pragma once

#include "engine/GameObject.hpp"

#include <jngl.hpp>

class Orb : public GameObject {
public:
	Orb(Vector2d);
	void step() override;
	void draw() const override;

private:
	jngl::Sprite sprite;
	int frameCounter = 0;
};
