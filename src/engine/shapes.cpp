#include "shapes.hpp"

#include "GameObject.hpp"

Circle::Circle(const GameObject& parent, int radius) : Shape(parent), radius_(radius) {
}

Line::Line(const GameObject& parent, Vector2d start, Vector2d end)
: Shape(parent), start_(start), end_(end) {
}

Point::Point(const GameObject& parent) : Shape(parent) {
}

Shape::Shape(const GameObject& parent) : parent_(parent) {
}

Shape::~Shape() {
}

Vector2d Shape::GetPosition() const {
	return parent_.GetPosition();
}

const GameObject& Shape::GetParent() const {
	return parent_;
}

NullShape::NullShape(const GameObject& parent) : Shape(parent){};

Quadrat::Quadrat(const GameObject& parent, int size)
: Shape(parent), size_(size), top_(parent, Vector2d(size, size) / 2, Vector2d(-size, size) / 2),
  bottom_(parent, Vector2d(size, -size) / 2, Vector2d(-size, -size) / 2),
  left_(parent, Vector2d(size, size) / 2, Vector2d(size, -size) / 2),
  right_(parent, Vector2d(-size, size) / 2, Vector2d(-size, -size) / 2) {
}
