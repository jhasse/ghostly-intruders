#include "collision.hpp"

#include "../Ghost.hpp"
#include "../Game.hpp"
#include "../CollisionLine.hpp"
#include "../Well.hpp"
#include "../Player.hpp"
#include "../Parent.hpp"
#include "../Kid.hpp"
#include "../Beam.hpp"
#include "../DeadGhost.hpp"
#include "../Orb.hpp"

#include <stdexcept>

CheckCollisionExecutor::CheckCollisionExecutor() : collided_(false) {
}

void CheckCollisionExecutor::Fire(const Circle& circle, const Point& point) {
	if ((circle.GetPosition() - point.GetPosition()).LengthSq() <=
	    circle.GetRadius() * circle.GetRadius()) {
		SetCollisionPoint(point.GetPosition());
	}
}

void CheckCollisionExecutor::Fire(const Line& line, const Point& point) {
	Circle circle(point.GetParent(), 5);
	return Fire(circle, line);
}

void CheckCollisionExecutor::Fire(const Circle& circle, const Line& line) {
	const Vector2d connection = line.getAbsEnd() - line.getAbsStart();
	double dist0 = (circle.GetPosition() - line.getAbsStart()) * connection.Normalize();
	if (dist0 > 0 && dist0 < connection.Length()) {
		if ((line.getAbsStart() + (dist0 * connection.Normalize()) - circle.GetPosition())
		        .LengthSq() < circle.GetRadius() * circle.GetRadius()) {
			SetCollisionPoint(line.getAbsStart() + (dist0 * connection.Normalize()));
			return; // true
		}
	}

	if ((circle.GetPosition() - line.getAbsStart()).LengthSq() <
	    circle.GetRadius() * circle.GetRadius()) {
		SetCollisionPoint(line.getAbsStart());
		return; // true
	}
	if ((circle.GetPosition() - line.getAbsEnd()).LengthSq() <
	    circle.GetRadius() * circle.GetRadius()) {
		SetCollisionPoint(line.getAbsEnd());
		return; // true
	}
	return;
}

void CheckCollisionExecutor::Fire(const Quadrat& quad, const Point& point) {
	Vector2d temp = point.GetPosition() + Vector2d(quad.GetSize(), quad.GetSize()) / 2;
	if (quad.GetPosition().X() < temp.X() &&
	    quad.GetPosition().X() + quad.GetSize() > temp.X() &&
	    quad.GetPosition().Y() < temp.Y() &&
	    quad.GetPosition().Y() + quad.GetSize() > temp.Y()) {
		SetCollisionPoint(point.GetPosition());
		return; // true
	}
	return;
}

void CheckCollisionExecutor::Fire(const Circle& circle, const Quadrat& quad) {
	Fire(circle, quad.GetTop());
	Fire(circle, quad.GetBottom());
	Fire(circle, quad.GetLeft());
	Fire(circle, quad.GetRight());
	//	Point temp(circle.GetParent());
	//	Fire(quad, temp);
}

void CheckCollisionExecutor::Fire(const Quadrat& quad, const Line& line) {
	Fire(line, quad.GetTop());
	Fire(line, quad.GetBottom());
	Fire(line, quad.GetLeft());
	Fire(line, quad.GetRight());
	//	Point temp(line.GetParent());
	//	Fire(quad, temp);
}

void CheckCollisionExecutor::Fire(const Circle& circle, const Circle& circle2) {
	if ((circle.GetPosition() - circle2.GetPosition()).LengthSq() <=
	    (circle.GetRadius() + circle2.GetRadius()) *
	        (circle.GetRadius() + circle2.GetRadius())) {
		SetCollisionPoint(circle.GetPosition() +
		                  (circle2.GetPosition() - circle.GetPosition()).Normalize() *
		                      circle.GetRadius());
	}
}

void CheckCollisionExecutor::Fire(const Line& lhs, const Line& rhs) {
	const double s1x = lhs.getAbsStart().X();
	const double s1y = lhs.getAbsStart().Y();
	const Vector2d direction1 = lhs.getAbsEnd() - lhs.getAbsStart();
	const double r1x = direction1.X();
	const double r1y = direction1.Y();
	const double s2x = rhs.getAbsStart().X();
	const double s2y = rhs.getAbsStart().Y();
	const Vector2d direction2 = rhs.getAbsEnd() - rhs.getAbsStart();
	const double r2x = direction2.X();
	const double r2y = direction2.Y();
	/*	if(abs(r1x * r2y - r1y * r2x) <= 0.01 && abs(r1x * r2y - r1y * r2x) <= 0.01) //
	   parallel
	        {
	                if(abs(s1x - s2x) <= 2 || abs(s1y - s2y) <= 2)
	                {
	                        std::cout << "p" << std::flush;
	                        return;
	                }
	                return;
	        }*/
	const double u = (r2x * (s1y - s2y) - r2y * (s1x - s2x)) / (r1x * r2y - r1y * r2x);
	if (u >= 0 && u <= 1) {
		const double v = (r1x * (s1y - s2y) - r1y * (s1x - s2x)) / (r1x * r2y - r1y * r2x);
		if (v >= 0 && v <= 1) {
			SetCollisionPoint(lhs.getAbsStart() + direction1 * u);
			return; // true
		}
	}
	return;
}

void CheckCollisionExecutor::Fire(const Quadrat& q1, const Quadrat& q2) {
	Fire(q1, q2.GetTop());
	Fire(q1, q2.GetBottom());
	Fire(q1, q2.GetLeft());
	Fire(q1, q2.GetRight());
}

void CheckCollisionExecutor::Fire(const Shape&, const Shape&) {
}

void CheckCollisionExecutor::Fire(const Shape&, const NullShape&) {
}

void CheckCollisionExecutor::Fire(const NullShape&, const Shape&) {
}

void CheckCollisionExecutor::Fire(const NullShape&, const NullShape&) {
}

void CheckCollisionExecutor::OnError(const Shape&, const Shape&) {
	throw std::runtime_error("Error on checking collision!");
}

bool CheckCollisionExecutor::Collided() const {
	return collided_;
}

void CheckCollisionExecutor::SetCollided(bool c) {
	collided_ = c;
}

Vector2d CheckCollisionExecutor::GetCollisionPoint() const {
	assert(collided_);
	return collisionPoint_;
}

void CheckCollisionExecutor::SetCollisionPoint(const Vector2d& point) {
	collisionPoint_ = point;
	collided_ = true;
}

HandleCollisionExecutor::HandleCollisionExecutor(Game& game) : game_(game) {
}

void HandleCollisionExecutor::Fire(Parent& player, Parent& enemy) {
	const Vector2d connection0 = (enemy.GetPosition() - player.GetPosition()).Normalize();
	const double enemyImpulse = connection0 * enemy.GetSpeed() * -1;
	const double impulse = connection0 * player.GetSpeed();

	player.Move(-1 * connection0 * (std::abs(impulse) + 0.1));
	enemy.Move(connection0 * (std::abs(enemyImpulse) + 0.1));

	player.Accelerate(enemyImpulse * -0.8 * connection0); // I get the enemy's impulse
	enemy.Accelerate(enemyImpulse * 0.8 * connection0);   // Enemy loses his impulse

	enemy.Accelerate(impulse * 0.8 * connection0);   // Enemy gets my impulse
	player.Accelerate(impulse * -0.8 * connection0); // I lose my impulse
}

void HandleCollisionExecutor::Fire(Player& ship, CollisionRectangle& quad) {
	Fire(ship, GetCollisionPoint());
	quad.SetCollision(true);
}

void HandleCollisionExecutor::Fire(Parent& parent, Kid&) {
	Fire(parent, GetCollisionPoint());
}

void HandleCollisionExecutor::Fire(Ghost&, Kid& kid) {
	kid.attack();
}

void HandleCollisionExecutor::Fire(GameObject& a, GameObject& b) {
	return OnError(a, b);
}

void HandleCollisionExecutor::OnError(GameObject&, GameObject&) {
	// Do nothing
}

void HandleCollisionExecutor::Fire(Parent& ship, CollisionLine& line) {
	Fire(ship, GetCollisionPoint());
	line.SetCollision(true);
}

void HandleCollisionExecutor::Fire(Ghost& ship, ImpassableWall& line) {
	Fire(ship, GetCollisionPoint());
	line.SetCollision(true);
}

void HandleCollisionExecutor::Fire(DeadGhost& ship, ImpassableWall& line) {
	Fire(ship, GetCollisionPoint());
	line.SetCollision(true);
}

void HandleCollisionExecutor::Fire(Player& ship, const Vector2d& point) {
	Vector2d connection = ship.GetPosition() - point;
	const double distance = connection.Length();
	connection = connection.Normalize();
	ship.blockMovementFrom(connection);
	const double impulse = connection * ship.GetSpeed();
	ship.Accelerate(impulse * -(1.0) * connection); // Faktor bestimmt, wie stark man abprallt
	ship.Move(connection *
	          (std::abs(impulse) + std::max(0.0, (ship.GetRadius() - distance) * 0.2)));
}

void HandleCollisionExecutor::Fire(Parent& parent, const CollisionCircle&) {
	// Der Brunnen ist ein CollisionCircle, Eltern sollten davon abprallen
	Fire(parent, GetCollisionPoint());
}

void HandleCollisionExecutor::Fire(Ghost& player, Well& well) {
	// Man soll nicht zu schnell vom Brunnen eingesogen werden, daher noch dieser Check:
	if ((player.GetPosition() - well.GetPosition()).LengthSq() < 25 * 25) {
		well.teleport(player);
	}
}

void HandleCollisionExecutor::Fire(Ghost& ghost, Beam&) {
	ghost.attack();
}

void HandleCollisionExecutor::Fire(Parent&, Beam&) {
	// Lichstrahl geht durch die Eltern
}

void HandleCollisionExecutor::Fire(GameObject& gameObject, Beam& beam) {
	if (gameObject.getZIndex() < 99999) {
		beam.collisionAt(GetCollisionPoint());
	}
}

void HandleCollisionExecutor::Fire(DeadGhost& deadGhost, Orb& orb) {
	deadGhost.collectOrb();
	game_.Remove(&orb);
}

void HandleCollisionExecutor::Fire(Beam&, Beam&) {
}

void HandleCollisionExecutor::Fire(Kid&, Beam&) {
}

void HandleCollisionExecutor::SetCollisionPoint(const Vector2d& point) {
	collisionPoint_ = point;
}

Vector2d HandleCollisionExecutor::GetCollisionPoint() const {
	return collisionPoint_;
}
