#include "GameObject.hpp"

GameObject::GameObject() : shape_(new NullShape(*this)) {
}

Vector2d GameObject::GetPosition() const {
	return position;
}

void GameObject::SetGame(Game* game) {
	game_ = game;
}

void GameObject::setShape(Shape* shape) {
	shape_.reset(shape);
}

Game& GameObject::GetGame() {
	return *game_;
}

Game* GameObject::game_ = nullptr;

const Shape& GameObject::GetShape() const {
	return *shape_;
}

double GameObject::getZIndex() const {
	if (zIndex) { return *zIndex; } else { return position.y; }
}

void GameObject::overwriteZIndex(const double zIndex) {
	this->zIndex = zIndex;
}

Shape& GameObject::getShape() {
	return *shape_;
}

void MoveableObject::Accelerate(const Vector2d& acc) {
	acc_ += acc;
}

Vector2d MoveableObject::GetSpeed() const {
	return speed;
}

void MoveableObject::Move(const Vector2d& mov) {
	position += mov;
}

void MoveableObject::StepMovement() {
	speed += acc_;
	acc_.Set(0, 0);
	speed += Vector2d(0, 0.2);
	speed *= 0.95;
	position += speed;
}
