#include "Screen.hpp"

#include "Paths.hpp"
#include "Options.hpp"

Screen::Screen() {
	xCenter = double(jngl::getWindowWidth()) / 2;
	yCenter = double(jngl::getWindowHeight()) / 2;
}

void Screen::draw(const std::string& filename, const double x, const double y) {
	jngl::draw(GetPaths().Graphics() + filename, x, y);
}

void Screen::drawScaled(const std::string& filename, const double x, const double y, const float xfactor, const float yfactor) {
	jngl::drawScaled(GetPaths().Graphics() + filename, x, y, xfactor, yfactor);
}

void Screen::drawCentered(const std::string& filename, const double x, const double y) {
	const std::string filepath = GetPaths().Graphics() + filename;
	jngl::draw(filepath,
	           x - jngl::getWidth(filepath) / 2,
	           y - jngl::getHeight(filepath) / 2);
}

void Screen::drawCentered(const std::string& filename, const Vector2d& position) {
	GetScreen().drawCentered(filename, position.X(), position.Y());
}

void Screen::drawCenteredScaled(const std::string& filename, const double x, const double y, const float factor) {
	const std::string filepath = GetPaths().Graphics() + filename;
	jngl::drawScaled(filepath, x - jngl::getWidth(filepath) / 2.0 * factor,
	                 y - jngl::getHeight(filepath) / 2.0 * factor, factor);
}

void Screen::drawCenteredScaled(const std::string& filename, const double x, const double y, const float xfactor, const float yfactor) {
	const std::string filepath = GetPaths().Graphics() + filename;
	jngl::drawScaled(filepath, x - jngl::getWidth(filepath) / 2.0 * xfactor,
	                 y - jngl::getHeight(filepath) / 2.0 * yfactor, xfactor, yfactor);
}

void Screen::drawLine(const Vector2d& start, const Vector2d& end) {
	return jngl::drawLine(start.X(), start.Y(), end.X(), end.Y());
}

Screen& GetScreen() {
	return *Screen::handle();
}

void Screen::print(const std::string& text, Vector2d v) {
	jngl::print(text, static_cast<int>(v.X()), static_cast<int>(v.Y()));
}

void Screen::printCentered(const std::string& text, const Vector2d& v) {
	printCentered(text, v.X(), v.Y());
}

void Screen::printCentered(const std::string& text, double x, double y) {
	jngl::print(text,
	            static_cast<int>((x - jngl::getTextWidth(text) / 2)),
	            static_cast<int>(y) - jngl::getFontSize() / 2);
}

void Screen::drawScaled(const std::string& filename, const double x, const double y, const float factor) {
	jngl::drawScaled(GetPaths().Graphics() + filename, x, y, factor);
}

int Screen::getMouseX() const {
	return jngl::getMousePos().x;
}

int Screen::getMouseY() const {
	return jngl::getMousePos().y;
}

Vector2d GetWindowVector() {
	return Vector2d(getOptions().get<int>("windowWidth"), getOptions().get<int>("windowHeight"));
}

Vector2d Screen::getAbsoluteMousePos() const {
	return Vector2d(getMouseX(), getMouseY());
}
