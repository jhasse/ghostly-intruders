#include "ResizeGraphics.hpp"

#include "Paths.hpp"
#include "Options.hpp"

#include <boost/lexical_cast.hpp>
#include <thread>

void ScanPath(boost::filesystem::path path, std::deque<std::string>& filesToResize) {
	boost::filesystem::directory_iterator end;
	for (boost::filesystem::directory_iterator it(path); it != end; ++it) {
		if (boost::filesystem::is_directory(it->status())) {
			ScanPath(it->path(), filesToResize);
		} else {
			std::string file = it->path().string();
			const std::string extension = ".webp";
			if (file.substr(file.size() - extension.size()) == extension) {
				filesToResize.push_back(std::string(it->path().string())
				                            .erase(0, jngl::getPrefix().size()));
			}
		}
	}
}

ResizeGraphics::ResizeGraphics() : originalSize_(-1) {
	boost::filesystem::path path(jngl::getPrefix() + GetPaths().data() + "gfx");
	boost::filesystem::directory_iterator end;
	for (boost::filesystem::directory_iterator it(path); it != end; ++it) {
		if (boost::filesystem::is_directory(it->status())) {
			std::string name = it->path().string(); // e.g. /gfx/x1200
			try {
				auto tmp = boost::lexical_cast<int>(name.substr(name.rfind("x") + 1));
				if (tmp > originalSize_) {
					originalSize_ = tmp;
					jngl::debug("Original screen height: "); jngl::debugLn(originalSize_);
				}
			} catch (boost::bad_lexical_cast&) {
				// Bad cast, this doesn't seem to be the right directory
			}
		}
	}

	const std::string origGfx = GetPaths().data() + "gfx/x" + std::to_string(originalSize_) + "/";
	GetPaths().SetOriginalGfx(origGfx);
	jngl::setScaleFactor(double(jngl::getWindowHeight()) / double(originalSize_));
	GetPaths().SetGraphics(origGfx);
	ScanPath(jngl::getPrefix() + GetPaths().data() + "gfx/x" + std::to_string(originalSize_),
	         filesToResize_);
}

bool ResizeGraphics::isFinished(float& percentage) {
	if (filesToResize_.empty()) {
		return true;
	}

	static size_t numberOfImages = filesToResize_.size();
	percentage = float(100 - filesToResize_.size() * 100 / numberOfImages);

	// Don't do anything in the first frame in order to draw the loading screen for the first time
	static bool firstFrame = true;
	if(firstFrame) {
		firstFrame = false;
		return false;
	}

	std::string basedir;
	if (jngl::getPrefix() == "") {
		basedir = GetPaths().data() + "gfx/x" + std::to_string(originalSize_);
	} else {
		basedir = "gfx/x" + std::to_string(originalSize_);
	}

	// Im Destruktor von Finally wird sichergestellt, dass die Sprites wirklich geladen wurden.
	// Wir erstellen deswegen mehrere, damit der Teil des Ladens, der nicht auf dem Main-Thread
	// ausgeführt werden muss, parallelisert werden kann.
	std::vector<jngl::Finally> loaders;
	for (size_t i = 0; i < std::thread::hardware_concurrency(); ++i) {
		std::string relativeFilename(filesToResize_[0].substr(basedir.size() + 1));
		std::string newFilename = GetPaths().Graphics() + relativeFilename;

		loaders.emplace_back(jngl::load(newFilename.substr(0, newFilename.size() - 5)));
		filesToResize_.pop_front();
		if (filesToResize_.empty()) {
			return true;
		}
	}
	return false;
}
