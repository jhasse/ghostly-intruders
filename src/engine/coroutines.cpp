#include "coroutines.hpp"

push_type::push_type(std::function<void()> yield) : yield(std::move(yield)) {
}

void push_type::operator()() {
	yield();
}

pull_type::pull_type(std::function<void(push_type&)> routine) {
	std::unique_lock<std::mutex> lock(mutex);
	thread.reset(new std::thread([this, routine]() {
		push_type tmp{ std::bind(&pull_type::yield, this) };
		try {
			routine(tmp);
		} catch (InterruptedException&) {
			return;
		} catch (std::runtime_error& e) {
			exception = std::move(e);
		}
		std::lock_guard<std::mutex> lock(mutex);
		quit = true;
		conditionVar.notify_all();
	}));
	conditionVar.wait(lock); // wait for the thread to start
}

pull_type::~pull_type() {
	{
		std::lock_guard<std::mutex> _(mutex);
		quit = true;
	}
	conditionVar.notify_all();
	thread->join();
}

void pull_type::yield() {
	std::unique_lock<std::mutex> lock(mutex);
	conditionVar.notify_all();
	conditionVar.wait(lock);
	if (quit) {
		throw InterruptedException();
	}
}

void pull_type::operator()() {
	std::unique_lock<std::mutex> lock(mutex);
	if (quit) {
		throw std::runtime_error("Coroutine has quit"); // TODO: Wie macht Boost.Coroutine das?
	}
	conditionVar.notify_all();
	conditionVar.wait(lock);
	if (exception) {
		throw *exception;
	}
}
