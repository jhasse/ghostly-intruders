#pragma once

#include "GameObject.hpp"

class Draw;

class Animation : public GameObject {
public:
	Animation(const std::string& foldername, unsigned int canon = 1, bool repeat = true);
	Animation(const Animation&) = delete;
	void draw() const override;
	void step() override;
	bool Finished();

	/// Springt direkt zu einem bestimmten Frame
	void setFrame(unsigned int);

	void setPosition(Vector2d);

private:
	std::string foldername_;
	int stepsPerFrame_;
	bool finished_;
	unsigned int numberOfFrames = 0;
	unsigned int currentFrame = 0;
	int stepsLeft_;
	std::string CreateFilename(int) const;
	bool repeat;
	const unsigned int canon;
};
