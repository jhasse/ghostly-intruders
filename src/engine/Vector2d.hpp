#pragma once

#include "s11n.hpp"
#include "dll.hpp"

#include <iosfwd>

class DllExport Vector2d {
public:
	Vector2d(double x, double y);
	explicit Vector2d(double v);
	Vector2d();
	double X() const;
	double Y() const;
	void Set(double x, double y);
	void SetX(double x);
	void SetY(double y);
	Vector2d& operator/=(const double v);
	Vector2d& operator*=(const double v);
	Vector2d& operator-=(const Vector2d& rhs);
	Vector2d& operator+=(const Vector2d& rhs);
	double Length() const;
	double LengthSq() const;
	double Sum() const;
	Vector2d Normalize() const;
	double toDegree() const;

	double x;
	double y;

private:
	friend class boost::serialization::access;
	template <class Archive> void serialize(Archive& ar, const unsigned int) {
		ar& x;
		ar& y;
	}
};

BOOST_CLASS_EXPORT_KEY(Vector2d)

bool operator!=(const Vector2d& lhs, const Vector2d& rhs);
DllExport Vector2d operator*(const Vector2d& lhs, const double v);
Vector2d operator/(const Vector2d& lhs, const double v);
DllExport Vector2d operator*(const double v, const Vector2d& rhs);
Vector2d operator/(const double v, const Vector2d& rhs);
double operator*(const Vector2d& lhs, const Vector2d& rhs);
DllExport Vector2d operator-(const Vector2d& lhs, const Vector2d& rhs);
Vector2d operator+(const Vector2d& lhs, const Vector2d& rhs);
std::ostream& operator<<(std::ostream& out, const Vector2d& vec);
