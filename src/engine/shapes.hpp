#pragma once

#include "Vector2d.hpp"

class GameObject;

class Shape {
public:
	Shape(const GameObject&);
	virtual ~Shape();
	Vector2d GetPosition() const;
	const GameObject& GetParent() const;

private:
	const GameObject& parent_;
};

class NullShape : public Shape {
public:
	NullShape(const GameObject&);
};

class Circle : public Shape {
public:
	Circle(const GameObject&, int radius);
	int GetRadius() const {
		return radius_;
	}
	void SetRadius(int radius) {
		radius_ = radius;
	}

protected:
	int radius_;
};

class Line : public Shape {
public:
	Line(const GameObject& parent, Vector2d start, Vector2d end);

	Vector2d getAbsStart() const {
		return GetPosition() + start_;
	}
	Vector2d getRelStart() const {
		return start_;
	}
	void setRelStart(Vector2d start) {
		start_ = start;
	}

	Vector2d getAbsEnd() const {
		return GetPosition() + end_;
	}
	Vector2d getRelEnd() const {
		return end_;
	}
	void setRelEnd(Vector2d end) {
		end_ = end;
	}

	double getLength() const {
		return (end_ - start_).Length();
	}

private:
	Vector2d start_, end_;
};

class Point : public Shape {
public:
	Point(const GameObject&);
};

class Quadrat : public Shape {
public:
	Quadrat(const GameObject&, int size);
	int GetSize() const {
		return size_;
	}
	Line GetTop() const {
		return top_;
	}
	Line GetLeft() const {
		return left_;
	}
	Line GetBottom() const {
		return bottom_;
	}
	Line GetRight() const {
		return right_;
	}

protected:
	int size_;
	Line top_, bottom_, left_, right_;
};
