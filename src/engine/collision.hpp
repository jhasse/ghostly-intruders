#pragma once

#include "../collisionobjects.hpp"

class Ghost;
class CollisionLine;
class ImpassableWall;
class Well;
class Player;
class Parent;
class Kid;
class Beam;
class Orb;
class DeadGhost;

class CheckCollisionExecutor {
public:
	void Fire(const Circle&, const Point&);
	void Fire(const Circle&, const Line&);
	void Fire(const Circle&, const Circle&);
	void Fire(const Circle&, const Quadrat&);
	void Fire(const Quadrat&, const Point&);
	void Fire(const Quadrat&, const Line&);
	void Fire(const Quadrat&, const Quadrat&);
	void Fire(const Line&, const Line&);
	void Fire(const Shape&, const Shape&);
	void Fire(const Shape&, const NullShape&);
	void Fire(const NullShape&, const Shape&);
	void Fire(const NullShape&, const NullShape&);
	void Fire(const Line& line, const Point&);
	void OnError(const Shape&, const Shape&);
	CheckCollisionExecutor();
	bool Collided() const;
	void SetCollided(bool c);
	Vector2d GetCollisionPoint() const;

private:
	void SetCollisionPoint(const Vector2d&);
	Vector2d collisionPoint_;
	bool collided_;
};

class HandleCollisionExecutor {
public:
	void Fire(Parent&, Parent&);
	void Fire(GameObject&, GameObject&);
	void Fire(Parent&, CollisionLine&);
	void Fire(Ghost&, ImpassableWall&);
	void Fire(DeadGhost&, ImpassableWall&);
	void Fire(Player&, CollisionRectangle&);
	void Fire(Parent&, Kid&);
	void Fire(Ghost&, Kid&);
	void Fire(Player&, const Vector2d& point);
	void Fire(Parent&, const CollisionCircle&);
	void Fire(Ghost&, Well&);
	void Fire(Ghost&, Beam&);
	void Fire(Parent&, Beam&);
	void Fire(GameObject&, Beam&);
	void Fire(DeadGhost&, Orb&);
	void Fire(Beam&, Beam&);
	void Fire(Kid&, Beam&);
	void OnError(GameObject&, GameObject&);
	HandleCollisionExecutor(Game&);
	void SetCollisionPoint(const Vector2d&);

private:
	Vector2d GetCollisionPoint() const;
	Game& game_;
	Vector2d collisionPoint_;
};
