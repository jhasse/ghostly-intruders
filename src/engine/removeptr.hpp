#pragma once

#include <algorithm>

template<class T>
class PointerCompare {
public:
	PointerCompare(const T* const p) : p(p) {}
	bool operator()(const T& o) {
		return &o == p;
	}
	bool operator()(const T* o) {
		return o == p;
	}
private:
	const T* const p;
};

template<class T, class U>
void removePtr(T& container, const U* objectToRemove) {
	PointerCompare<U> c(objectToRemove);
	container.erase(std::find_if(container.begin(), container.end(), c));
}
