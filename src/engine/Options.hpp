#pragma once

#include "Singleton.hpp"
#include "dll.hpp"

#include <boost/serialization/map.hpp>
#include <boost/serialization/variant.hpp>
#include <jngl.hpp>

class Options : public Singleton<Options> {
public:
	Options();
	DllExport void Save() const;

	template<class T>
	T get(const std::string& name) const {
		try {
			return boost::get<T>(values_.at(name));
		} catch(std::exception& e) {
			jngl::debug("Error loading "); jngl::debug(name);
			jngl::debug(": "); jngl::debugLn(e.what());
			return 0;
		}
	}
	template<class T>
	void Set(const std::string& name, const T& value) {
		values_[name] = value;
	}
	template<class T>
	void SetFallback(const std::string& name, T fallback) {
		auto it = values_.find(name);
		if (it == values_.end()) {
			Set(name, fallback);
		}
	}
	void SetFallback(const std::string& name, const char* fallback) {
		auto it = values_.find(name);
		if (it == values_.end()) {
			Set(name, std::string(fallback));
		}
	}
private:
	std::map<std::string, boost::variant<int, std::string, float, bool>> values_;
	std::string filename_;

	friend class boost::serialization::access;
	template <class archive> void serialize(archive& ar, const unsigned int /*version*/) {
		using boost::serialization::make_nvp;
		ar & make_nvp("values", values_);
	}
};

DllExport Options& getOptions(); // Easier access to the singleton
