#pragma once

#include "shapes.hpp"
#include "dll.hpp"

#include <list>
#include <memory>
#include <optional>

class Game;
class Video;

class DllExport GameObject {
public:
	DllExport GameObject();
	virtual ~GameObject() = default;
	virtual void step() = 0;
	virtual void draw() const = 0;
	DllExport virtual Vector2d GetPosition() const;
	static void SetGame(Game*);
	virtual const Shape& GetShape() const;
	double getZIndex() const;
	void overwriteZIndex(double);

protected:
	Vector2d position;
	DllExport static Game& GetGame();
	Shape& getShape();
	void setShape(Shape*);

private:
	static Game* game_;
	std::unique_ptr<Shape> shape_;
	std::optional<double> zIndex;
};

class DllExport MoveableObject : public GameObject {
public:
	virtual void Accelerate(const Vector2d&);
	virtual Vector2d GetSpeed() const;
	virtual void Move(const Vector2d&);

protected:
	void StepMovement();
	Vector2d speed;
	Vector2d acc_;
};
