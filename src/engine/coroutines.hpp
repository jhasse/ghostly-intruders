#pragma once

// #include <boost/coroutine/all.hpp>

// using boost::coroutines::asymmetric_coroutine;
// using push_type = boost::coroutines::asymmetric_coroutine<void>::push_type;

#include "dll.hpp"

#include <thread>
#include <condition_variable>
#include <functional>
#include <optional>

class InterruptedException : public std::exception {};

class push_type {
public:
	push_type(std::function<void()> yield);
	DllExport void operator()();

private:
	std::function<void()> yield;
};

class pull_type {
public:
	pull_type(std::function<void(push_type&)> routine);
	~pull_type();
	void yield();
	DllExport void operator()();

private:
	std::unique_ptr<std::thread> thread;
	std::condition_variable conditionVar;
	std::mutex mutex;
	bool quit = false;
	std::optional<std::runtime_error> exception;
};
