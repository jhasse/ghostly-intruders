#include "Paths.hpp"

#include "Options.hpp"
#include "../constants.hpp"

#if defined (__linux__)
	#include "linux/binreloc.h"
#elif defined (__APPLE__)
	#ifndef __IPHONE_OS_VERSION_MIN_REQUIRED
		#include <mach-o/dyld.h>
		#include <CoreServices/CoreServices.h>
	#endif
#else
	#include <windows.h>
	#include <shlobj.h>
#endif
#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>

std::string Paths::resolutionGraphics() const {
	return (boost::format("%1%gfx/%2%x%3%/") % data() % jngl::getScreenWidth() % jngl::getScreenHeight()).str();
}

#ifndef __IPHONE_OS_VERSION_MIN_REQUIRED
Paths::Paths() {
#if defined(__linux__)
	BrInitError error;
	if (br_init(&error) == 0 && error != BR_INIT_ERROR_DISABLED) {
		std::cout << "Warning: BinReloc failed to initialize (error code " << error << ")\n"
		          << "Will fallback to hardcoded default path." << std::endl;
	}
	auto tmp = br_find_exe_dir("");
	boost::filesystem::current_path(tmp);
	free(tmp);
	std::stringstream path;
	path << getenv("HOME") << "/.config/" << programDisplayName << "/";
	configPath = path.str();
#elif defined(__APPLE__)
	uint32_t size = 0;
	if (_NSGetExecutablePath(NULL, &size) == 0) {
		throw std::runtime_error("Can't get executable path!");
	} else {
		std::vector<char> tmp(size);
		_NSGetExecutablePath(&tmp[0], &size);
		prefix.assign(tmp.begin(), tmp.end());
		boost::filesystem::path prefixPath(prefix);
		boost::filesystem::current_path(prefixPath.normalize().remove_leaf());
	}

	FSRef ref;
	FSFindFolder(kUserDomain, kApplicationSupportFolderType, kCreateFolder, &ref);
	char path[PATH_MAX];
	FSRefMakePath(&ref, (UInt8*)&path, PATH_MAX);
	boost::filesystem::path applicationSupportFolder(path);
	applicationSupportFolder /= programDisplayName;
	configPath = applicationSupportFolder.string() + "/";
#else
	char filename[MAX_PATH];
	GetModuleFileName(NULL, filename, MAX_PATH);
	std::string tmp(filename);
	boost::filesystem::current_path(boost::filesystem::path(filename).parent_path());

	TCHAR szPath[MAX_PATH];
	if (!SUCCEEDED(SHGetFolderPath(NULL, CSIDL_APPDATA | CSIDL_FLAG_CREATE, NULL, 0, szPath))) {
		throw std::runtime_error("Couldn't get %AppData% location!");
	}
	std::stringstream path;
	path << szPath << "/" << programDisplayName << "/";
	configPath = path.str();
#endif
	boost::filesystem::create_directory(configPath);
#else
Paths::Paths() : configPath(jngl::getConfigPath()) {
#endif
}

std::string Paths::Graphics()
{
	return graphics_;
}

std::string Paths::fonts() const {
	return data() + "fonts/";
}

void Paths::SetGraphics(const std::string& graphics)
{
	graphics_ = graphics;
}

std::string Paths::data() const {
	return "../data/";
}

std::string Paths::Config()
{
	return configPath;
}

Paths& GetPaths() {
	return *Paths::handle();
}

std::string Paths::OriginalGfx() const
{
	return originalGfx_;
}

void Paths::SetOriginalGfx(const std::string& originalGfx)
{
	originalGfx_ = originalGfx;
}
