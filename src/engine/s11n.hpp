#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/ptr_container/ptr_list.hpp>

typedef boost::archive::text_iarchive iarchive;
typedef boost::archive::text_oarchive oarchive;
