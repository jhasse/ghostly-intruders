#pragma once

#include <deque>
#include <boost/filesystem.hpp>

class ResizeGraphics {
public:
	ResizeGraphics();
	bool isFinished(float& percentage);

private:
	std::deque<std::string> filesToResize_;
	int originalSize_;
};

void ScanPath(boost::filesystem::path path, std::deque<std::string>& filesToResize);
