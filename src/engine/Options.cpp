#include "Options.hpp"

#include "Paths.hpp"

#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <fstream>

Options::Options() : filename_(GetPaths().Config() + "options.xml") {
	try {
		std::ifstream ifs(filename_.c_str());
		boost::archive::xml_iarchive xml(ifs);
		xml >> boost::serialization::make_nvp("options", *this);
	} catch(std::exception& e) {
		jngl::debug("Error loading ");
		jngl::debug(filename_);
		jngl::debug(": ");
		jngl::debugLn(e.what());
	}
}

void Options::Save() const {
	std::ofstream ofs(filename_.c_str());
	if (ofs) {
		boost::archive::xml_oarchive xml(ofs);
		xml << boost::serialization::make_nvp("options", *this);
	} else {
		jngl::errorMessage("Konnte in Datei " + filename_ + " nicht schreiben!");
	}
}

Options& getOptions() {
	return *Options::handle();
}
