#include "Gamepad.hpp"

#include <jngl/input.hpp>
#include <jngl/debug.hpp>

Gamepad::Gamepad(std::shared_ptr<jngl::Controller> controller) : controller(std::move(controller)) {
}

Vector2d Gamepad::getAcceleration() const {
	return Vector2d(controller->state(jngl::controller::LeftStickX),
	                -controller->state(jngl::controller::LeftStickY));
}

Vector2d Gamepad::getDirection() const {
	return Vector2d(controller->state(jngl::controller::RightStickX),
	                -controller->state(jngl::controller::RightStickY));
}

bool Gamepad::getShoot() const {
	return controller->down(jngl::controller::RightTrigger);
}
