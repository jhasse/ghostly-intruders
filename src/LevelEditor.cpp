#include "LevelEditor.hpp"

#include "Game.hpp"
#include "engine/Screen.hpp"
#include "gui/Button.hpp"

#include <jngl/input.hpp>

LevelEditor::LevelEditor(std::shared_ptr<Game> game, Level& level)
: game(std::move(game)), previousMouseVisibility(jngl::isMouseVisible()), level(level) {
	auto b = std::make_shared<Button>("BorderArea hinzufügen", [this]() {
		mouseType = MouseType::ADD_BORDER_AREA;
	});
	b->CenterAt(-850, -550);
	AddWidget(b);
	jngl::setMouseVisible(true);
}

LevelEditor::~LevelEditor() {
	jngl::setMouseVisible(previousMouseVisibility);
}

void LevelEditor::step() {
	switch (mouseType) {
	case MouseType::SELECT:
		StepWidgets();
		level.stepVertexGrabbers();
		break;
	case MouseType::ADD_BORDER_AREA:
		if (jngl::mousePressed(jngl::mouse::Left)) {
			level.addBorderArea(GetScreen().getAbsoluteMousePos());
			mouseType = MouseType::SELECT;
		}
		break;
	}

	if (jngl::keyPressed(jngl::key::F3)) {
		level.save();
		jngl::setWork(game);
	}
}

void LevelEditor::draw() const {
	game->draw();
	jngl::pushMatrix();
	level.drawVertexGrabbers();
	jngl::popMatrix();
	DrawWidgets();
}
