#include "BorderArea.hpp"

#include <jngl.hpp>

BOOST_CLASS_EXPORT_IMPLEMENT(BorderArea)

BorderArea::BorderArea(std::vector<Vector2d> vertices, std::vector<std::vector<Vector2d>> holes) {
	for (auto& vertex : vertices) {
		this->vertices.emplace_back(vertex);
	}
	for (auto& hole : holes) {
		std::vector<VertexGrabber> newHole;
		for (auto& vertex : hole) {
			newHole.emplace_back(vertex);
		}
		this->holes.emplace_back(std::move(newHole));
	}
}

bool BorderArea::step() {
	bool changed = false;
	for (auto it = vertices.begin(); it != vertices.end(); ++it) {
		std::optional<Vector2d> newVertex;
		if (it->step(newVertex)) {
			changed = true;
		}
		if (newVertex) {
			vertices.insert(it, std::move(*newVertex));
			break;
		}
	}
	return changed;
}

void BorderArea::draw() const {
	for (auto& vertexGrabber : vertices) {
		vertexGrabber.draw();
	}
	for (auto& hole : holes) {
		for (auto& vertexGrabber : hole) {
			vertexGrabber.draw();
		}
	}
}

void BorderArea::create(
    const std::function<void(std::vector<Vector2d> vertices,
                             std::vector<std::vector<Vector2d>> holes)>& callback) const {

	std::vector<Vector2d> plainVertices;
	for (const auto& vertexGrabber : vertices) {
		plainVertices.push_back(vertexGrabber.getVertex());
	}
	std::vector<std::vector<Vector2d>> plainHoles;
	for (auto& hole : holes) {
		std::vector<Vector2d> plainHole;
		for (const auto& vertexGrabber : hole) {
			plainHole.push_back(vertexGrabber.getVertex());
		}
	}
	callback(plainVertices, plainHoles);
}
