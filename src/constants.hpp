#pragma once

#include <string>

const std::string programShortName = "ghostly-intruders";
const std::string programDisplayName = "Ghostly Intruders";

const int wallThickness = 30;
