#pragma once

#include "Player.hpp"

#include "engine/Animation.hpp"

#include <jngl/sprite.hpp>
#include <array>

class Beam;

class Parent : public Player {
public:
	Parent(const Vector2d&, const int playerNr);
	virtual void draw() const override;
	virtual void step() override;
	virtual void HandleCollisionWithPoint(const Vector2d&);

private:
	int radius_;
	Animation* activeAnimation;
	std::array<Animation, 2> animations;
	Beam* beam = nullptr;

	const static unsigned int RUN_DURATION;
	const static unsigned int RUN_COOLDOWN;
	unsigned int runCountdown = 0;
};
