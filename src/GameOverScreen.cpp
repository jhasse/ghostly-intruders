#include "GameOverScreen.hpp"

#include "PauseMenu.hpp"
#include "fonts.hpp"

#include <jngl.hpp>

GameOverScreen::GameOverScreen(std::shared_ptr<Work> game, const bool ghostsWon)
: game(std::move(game)), text(ghostsWon ? "Ghostly Victory!" : "Parents won!") {
	text.setFont(*fonts::veryBig());
	text.setAlign(jngl::Alignment::CENTER);
	text.setCenter(0, -500);
}

void GameOverScreen::step() {
	zoomIn += (1.0 - zoomIn) / 30.0;
}

void GameOverScreen::draw() const {
	game->draw();
	jngl::setFontColor(0, 0, 0);
	jngl::setFontSize(120);
	jngl::setColor(255, 255, 255, 150 * zoomIn);
	jngl::drawRect(-jngl::getScreenWidth() / 2, -jngl::getScreenHeight() / 2,
	               jngl::getScreenWidth(), jngl::getScreenHeight());
	text.draw();
}

void GameOverScreen::onQuitEvent() {
	jngl::setWork(std::make_shared<PauseMenu>(jngl::getWork()));
	jngl::cancelQuit();
}
