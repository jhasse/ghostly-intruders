#pragma once

#include "engine/GameObject.hpp"

class CollisionRectangle : public GameObject {
public:
	CollisionRectangle(Vector2d position, int size);
	void step();
	void draw() const;
	void SetCollision(bool);

private:
	int size_;
	bool collision_;
};

class CollisionCircle : public GameObject {
public:
	CollisionCircle(Vector2d position, int radius);
	void step();
	void draw() const;
	void SetCollision(bool);

protected:
	int radius;

private:
	bool collision_;
};
