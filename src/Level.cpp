#include "Level.hpp"

#include "Game.hpp"
#include "Ghost.hpp"
#include "Parent.hpp"
#include "collisionobjects.hpp"
#include "CollisionLine.hpp"
#include "Well.hpp"
#include "Wall.hpp"
#include "Kid.hpp"
#include "Control.hpp"

#include <sstream>
#include <fstream>

Level::Level(Game& game) : game_(game) {
}

void Level::addBorderArea(Vector2d position) {
	std::vector<Vector2d> tmp;
	tmp.emplace_back(std::move(position));
	std::vector<std::vector<Vector2d>> tmp2;
	borderAreas.emplace_back(tmp, tmp2);
}

bool Level::stepVertexGrabbers() {
	bool changed = false;
	for (auto& borderArea : borderAreas) {
		if (borderArea.step()) {
			changed = true;
		}
	}
	return changed;
}

void Level::drawVertexGrabbers() const {
	for (auto& borderArea : borderAreas) {
		borderArea.draw();
	}
}

void Level::addPlayer(const double x, const double y) {
	const auto playerNr = game_.getNumberOfPlayers();
	if (playerNr % 2 == 0) {
		game_.add<Parent>(Vector2d(x, y), playerNr);
	} else {
		game_.add<Ghost>(Vector2d(x, y), playerNr);
	}
	game_.setNumberOfPlayers(playerNr + 1);
}

void Level::addWell(const double x, const double y) {
	game_.add<Well>(Vector2d(x, y));
}

void Level::save() const {
	std::string filename = game_.getLevelTxtPath();
	std::ofstream ofs(filename);
	if (ofs) {
		oarchive archive(ofs);
		try {
			archive & borderAreas;
		} catch (std::exception& e) {
			jngl::errorMessage(e.what());
		}
	} else {
		jngl::errorMessage("Konnte in Datei " + filename + " nicht schreiben!");
	}
}

void Level::addLine(double xstart, double ystart, double xend, double yend) {
	game_.add<CollisionLine>(Vector2d(xstart, ystart), Vector2d(xend, yend));
}

void Level::addImpassableWall(double xstart, double ystart, double xend, double yend) {
	game_.add<ImpassableWall>(Vector2d(xstart, ystart), Vector2d(xend, yend));
}

void Level::addQuad(const double x, const double y, const int size) {
	game_.add<CollisionRectangle>(Vector2d(x, y), size);
}

void Level::addCircle(const double x, const double y, const int radius) {
	game_.add<CollisionCircle>(Vector2d(x, y), radius);
}

void Level::addWall(const double x, const double y, const std::string& file,
                    std::vector<std::pair<double, double>> doors) {
	game_.add<Wall>(Vector2d(x, y), file, std::move(doors));
}

void Level::addKid(const double x, const double y) {
	game_.add<Kid>(Vector2d(x, y));
}

void Level::repeat(push_type& yield, const unsigned int times) {
	for (unsigned int i = 0; i < times; ++i) {
		yield();
	}
}

std::string Level::getTxtPath() const {
	return game_.getLevelTxtPath();
}
