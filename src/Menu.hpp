#pragma once

#include "engine/Work.hpp"
#include "gui/ButtonBox.hpp"

#include <jngl/sprite.hpp>

class Menu : public Work {
public:
	Menu();
	~Menu();
	void step() override;
	void draw() const override;

private:
	jngl::Sprite logo;

	const static std::string backgroundMusic;
};
