#include "VertexGrabber.hpp"

#include "../engine/Screen.hpp"
#include "../engine/Vector2d.hpp"

#include <jngl.hpp>

BOOST_CLASS_EXPORT_IMPLEMENT(VertexGrabber)

constexpr double GRAB_RADIUS = 16;

VertexGrabber::VertexGrabber(const Vector2d& vertex) : vertex(vertex) {
}

bool VertexGrabber::step(std::optional<Vector2d>& newVertex) {
	mouseOver = (GetScreen().getAbsoluteMousePos() - vertex).LengthSq() < GRAB_RADIUS * GRAB_RADIUS;
	if (mouseOver && jngl::mousePressed(jngl::mouse::Left)) {
		grabbed = true;
	}
	if (grabbed) {
		vertex = GetScreen().getAbsoluteMousePos();
	}
	if (mouseOver && jngl::keyPressed(jngl::key::AltL)) {
		this->newVertex = vertex;
	}
	if (this->newVertex) {
		this->newVertex = GetScreen().getAbsoluteMousePos();
	}
	if (this->newVertex && !jngl::keyDown(jngl::key::AltL)) {
		newVertex = this->newVertex;
		this->newVertex = {};
	}
	if (grabbed && !jngl::mouseDown(jngl::mouse::Middle)) {
		grabbed = false;
		return true;
	}
	return false;
}

void VertexGrabber::draw() const {
	jngl::setColor(255, 0, 0, mouseOver ? 220 : 140);
	jngl::drawEllipse(vertex.X(), vertex.Y(), GRAB_RADIUS, GRAB_RADIUS);
	if (newVertex) {
		jngl::setColor(0, 255, 0, 140);
		jngl::drawEllipse(newVertex->X(), newVertex->Y(), GRAB_RADIUS, GRAB_RADIUS);
	}
}

Vector2d VertexGrabber::getVertex() const {
	return vertex;
}
