#pragma once

#include "../engine/s11n.hpp"
#include "../engine/Vector2d.hpp"

#include <optional>
#include <vector>

class VertexGrabber {
public:
	VertexGrabber() = default;
	VertexGrabber(const Vector2d& vertex);

	/// returns true if the vertex has changed
	bool step(std::optional<Vector2d>&);

	void draw() const;

	Vector2d getVertex() const;

private:
	bool grabbed = false;
	bool mouseOver = false;
	Vector2d vertex;

	// Ein neuer Vertex der nach diesem eingefügt werden soll
	std::optional<Vector2d> newVertex;

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive& ar, const unsigned int) {
		double x = 0;
		double y = 0;
		ar & x;
		ar & y;
		vertex.Set(x, y);
	}
};

BOOST_CLASS_EXPORT_KEY(VertexGrabber)
