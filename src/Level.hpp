#pragma once

#include "engine/GameObject.hpp"
#include "SharedObject.hpp"
#include "BorderArea.hpp"
#include "engine/coroutines.hpp"

#include <unordered_map>
#include <vector>

class DllExport Level {
public:
	Level(Game& game);
	virtual ~Level() = default;
	virtual void step(push_type&) = 0;
	void addBorderArea(Vector2d position);
	void addPlayer(double x, double y);
	void addWell(double x, double y);
	bool stepVertexGrabbers();
	void drawVertexGrabbers() const;
	void save() const;

protected:
	void addLine(double xstart, double ystart, double xend, double yend);
	void addImpassableWall(double xstart, double ystart, double xend, double yend);
	void addQuad(double x, double y, int size);
	void addCircle(double x, double y, int radius);
	void addWall(double x, double y, const std::string& file,
	             std::vector<std::pair<double, double>> doors);
	void addKid(double x, double y);

	/// Helper function to yield for multiple frames
	void repeat(push_type& yield, unsigned int times);

	std::string getTxtPath() const;

private:
	Game& game_;
	std::unordered_map<std::string, SharedObject> enemyScripts;
	std::vector<BorderArea> borderAreas;
};

extern "C" {
	DllExport void* createLevel(void* game);
}
