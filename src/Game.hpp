#pragma once

#include "engine/Work.hpp"
#include "engine/GameObject.hpp"
#include "engine/coroutines.hpp"
#include "levels/Level1.hpp"

#include <jngl/Color.hpp>
#include <map>
#include <list>
#include <iostream>

template <class T> struct array : std::list<T*> {};

class Game : public Work, public std::enable_shared_from_this<Game> {
public:
	Game();
	virtual ~Game();
	void step() override;
	void draw() const override;
	void onQuitEvent() override;

	template<class T, class... Args>
	T* add(Args&&... args) {
		T* tmp = new T(std::forward<Args>(args)...);
		needToAdd_.push_back(tmp);
		return tmp;
	}

	void Add(GameObject*);
	void Remove(GameObject*);

	template<class T>
	array<T>& GetAll() {
		return *reinterpret_cast<array<T>* >(&gameObjectsByType_[typeid(T).name()]);
	}

	template<class T>
	const std::list<const T*>& GetAll() const {
		auto it = gameObjectsByType_.find(typeid(T).name());
		if (it == gameObjectsByType_.end()) {
			static array<const T> empty;
			return empty;
		}
		return *reinterpret_cast<const std::list<const T*>*>(&it->second);
	}

	void PlayerDies();
	void setNumberOfPlayers(int);
	int getNumberOfPlayers() const;
	std::string getLevelTxtPath() const;

private:
	typedef array<GameObject> gameObjectsType;
	gameObjectsType gameObjects_;
	array<GameObject> needToRemove_; // Stores pointer to objects, that need to be removed
	array<GameObject> needToAdd_; // Stores pointer to objects, that need to be added
	std::map<std::string, gameObjectsType> gameObjectsByType_;
	template<class T>
	void addByType(GameObject* object) {
		if (T* p = dynamic_cast<T*>(object)) {
			GameObject* object = reinterpret_cast<GameObject*>(p);
			gameObjectsByType_[std::string(typeid(T).name())].push_back(object);
		}
	}
	template<class T>
	void RemoveByType(GameObject* object) {
		if (T* p = dynamic_cast<T*>(object)) {
			GameObject* object = reinterpret_cast<GameObject*>(p);
			gameObjectsByType_[std::string(typeid(T).name())].remove(object);
		}
	}
	void addObjects();
	void removeObjects();
	SharedObject sharedObject;
	std::unique_ptr<Level> level;
	pull_type levelCoroutine;
	int numberOfPlayers = 0;

	jngl::Sprite background;
	jngl::Sprite foreground;
	jngl::Sprite walls;

	const static std::string backgroundMusic;

	double secondsLeft = 300;
};
