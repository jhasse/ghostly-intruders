#include "PauseMenu.hpp"

#include "Menu.hpp"
#include "engine/Fade.hpp"

#include <boost/bind.hpp>

PauseMenu::PauseMenu(std::shared_ptr<jngl::Work> work) : work_(work) {
	jngl::setMouseVisible(true);
	auto buttonBox = std::make_shared<ButtonBox>(0, 0);
	buttonBox->Add("Return to game", boost::bind(&PauseMenu::Continue, this));
	buttonBox->Add("Return to main menu", boost::bind(&PauseMenu::QuitToMenu, this));
	buttonBox->Add("Quit", jngl::quit);
	AddWidget(buttonBox);
}

PauseMenu::~PauseMenu() {
	jngl::setMouseVisible(false);
}

void PauseMenu::step() {
	if (fadeOut) {
		fadeIn = 1.0f - (1.0f - fadeIn + 0.001f) * 1.5f;
		if (fadeIn < 0.01f) {
			jngl::setWork(work_);
		}
	} else {
		if (jngl::keyPressed(jngl::key::Escape)) {
			Continue();
		} else {
			StepWidgets();
		}
		fadeIn = 1.0f - (1.0f - fadeIn) / 1.4f;
	}
}

void PauseMenu::Continue() {
	fadeOut = true;
}

void PauseMenu::draw() const {
	work_->draw();
	jngl::setColor(255, 255, 255, 120 * fadeIn);
	jngl::drawRect(-jngl::getScreenWidth() / 2, -jngl::getScreenHeight() / 2,
	               jngl::getScreenWidth(), jngl::getScreenHeight());
	jngl::setColor(255, 255, 255, 255);
	jngl::translate(0, (1 - fadeIn) * 1500);
	DrawWidgets();
}

void PauseMenu::QuitToMenu() const {
	jngl::setWork(std::make_shared<Fade>(std::make_shared<Menu>()));
}
