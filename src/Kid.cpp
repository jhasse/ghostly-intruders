#include "Kid.hpp"

#include "engine/Paths.hpp"

#include <jngl.hpp>

const int MAX_HP = 300;

Kid::Kid(Vector2d position)
: hp(MAX_HP), spriteSleeping(GetPaths().Graphics() + "Bett-zu"),
  spriteAwake(GetPaths().Graphics() + "Bett-auf"), animations{{
	Animation("zzz_cycle"),
	Animation("warning_cycle"),
	Animation("traenen", 3)
}} {
	spriteAwake.setCenter(position);
	spriteSleeping.setCenter(position);
	this->position = position;
	activeAnimation = &animations[0];
	setShape(new Quadrat(*this, 50));
}

void Kid::step() {
	const int previousHp = hp;
	if (hp > 0 && numberOfAttackingGhosts > 0) {
		// Animation ist nur 30 FPS, daher durch 2:
		hp -= numberOfAttackingGhosts + 1; // unten wird noch ++ gemacht
	} else {
		activeAnimation = &animations[0];
		activeAnimation->step();
	}
	if (hp <= 0) {
		activeAnimation = &animations[2];
		activeAnimation->step();
	} else if (hp < MAX_HP) {
		++hp;
		activeAnimation = &animations[1];
		activeAnimation->setFrame((MAX_HP - hp) / 2);
	}
	if (previousHp == MAX_HP && hp < MAX_HP) {
		jngl::play(GetPaths().data() + "sfx/waking.ogg");
	}
	if (previousHp > 0 && hp <= 0) {
		jngl::play(GetPaths().data() + "sfx/awake.ogg");
	}
	numberOfAttackingGhosts = 0; // Kollisions-Behandlung setzt dies wieder auf die richtige Anzahl
}

void Kid::draw() const {
	if (hp > 0) {
		spriteSleeping.draw();
	} else {
		spriteAwake.draw();
	}
	jngl::pushMatrix();
	if (hp <= 0) {
		jngl::translate(position);
	} else {
		jngl::translate(position + Vector2d(60, -155));
	}
	activeAnimation->draw();
	jngl::popMatrix();
}

void Kid::attack() {
	++numberOfAttackingGhosts;
}

bool Kid::isAwake() const {
	return hp <= 0;
}
