#pragma once

#include "engine/Work.hpp"
#include "BorderArea.hpp"

#include <optional>

class Game;
class Level;

class LevelEditor : public Work {
public:
	LevelEditor(std::shared_ptr<Game>, Level&);
	~LevelEditor();
	void step();
	void draw() const;

private:
	std::shared_ptr<Game> game;
	bool previousMouseVisibility;
	Level& level;
	std::optional<Vector2d> grabMousePos;

	enum class MouseType {
		SELECT,
		ADD_BORDER_AREA,
	};
	MouseType mouseType = MouseType::SELECT;
};
