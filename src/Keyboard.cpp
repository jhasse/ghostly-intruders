#include "Keyboard.hpp"

#include <jngl/input.hpp>

Vector2d Keyboard::getAcceleration() const {
	Vector2d acc;
	if (jngl::keyDown(jngl::key::Up) || jngl::keyDown('w')) {
		acc += Vector2d(0, -1);
	}
	if (jngl::keyDown(jngl::key::Left) || jngl::keyDown('a')) {
		acc += Vector2d(-1, 0);
	}
	if (jngl::keyDown(jngl::key::Down) || jngl::keyDown('s')) {
		acc += Vector2d(0, 1);
	}
	if (jngl::keyDown(jngl::key::Right) || jngl::keyDown('d')) {
		acc += Vector2d(1, 0);
	}
	if (acc != Vector2d(0, 0)) {
		return acc.Normalize();
	}
	return acc;
}

Vector2d Keyboard::getDirection() const {
	return Vector2d(0, 0);
}

bool Keyboard::getShoot() const {
	return jngl::keyDown(jngl::key::ControlR);
}
