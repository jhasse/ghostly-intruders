#pragma once

#include <jngl/work.hpp>

class ReloadGame : public jngl::Work {
public:
	void step() override;
	void draw() const override;
};
