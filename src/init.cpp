#include "init.hpp"

#include "engine/Options.hpp"
#include "constants.hpp"
#include "Intro.hpp"

bool showWindow() {
	const Options& options = getOptions();
	try {
		jngl::hideWindow();
		jngl::showWindow(programDisplayName,
		                 options.get<int>("windowWidth"),
		                 options.get<int>("windowHeight"),
		                 options.get<int>("fullscreen") != 0,
		                 {16, 9}, {16, 9});
	}
	catch(std::runtime_error& err) {
		jngl::errorMessage(err.what());
		return false;
	}
	return true;
}

bool init() {
#ifdef NDEBUG
	try {
#endif
		getOptions().SetFallback("windowWidth", jngl::getDesktopWidth());
		getOptions().SetFallback("windowHeight", jngl::getDesktopHeight());
		getOptions().SetFallback("fullscreen", 1);
		getOptions().SetFallback("volume", 1.0f);
		getOptions().SetFallback("sound", true);
		if (jngl::getDesktopHeight() > 640 && !showWindow() && getOptions().get<int>("fullscreen")) {
			// Try again without fullscreen
			getOptions().Set("fullscreen", 0);
			showWindow();
		}
		jngl::setAntiAliasing(true);
		jngl::setWork(std::make_shared<Intro>());
#ifdef NDEBUG
	} catch (std::exception& e) {
		jngl::errorMessage(e.what());
		return false;
	}
#endif
	return true;
}
