#pragma once

#include "engine/Work.hpp"
#include "gui/Button.hpp"

class OptionsMenu : public Work {
public:
	OptionsMenu();
	void step() override;
	void draw() const override;
private:
	std::vector<Button> buttons;
};
