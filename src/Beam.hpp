#pragma once

#include "engine/GameObject.hpp"

#include <jngl/sprite.hpp>

class Beam : public GameObject {
public:
	Beam(Vector2d start);
	void step() override;
	void draw() const override;
	void setPositionAndDirection(Vector2d position, Vector2d direction);
	void collisionAt(Vector2d);

private:
	jngl::Sprite sprite;
	unsigned int length;
	Vector2d direction;
};
