#include "Game.hpp"

#include "GameOverScreen.hpp"
#include "Parent.hpp"
#include "Ghost.hpp"
#include "engine/collision.hpp"
#include "engine/Paths.hpp"
#include "engine/Options.hpp"
#include "engine/Screen.hpp"
#include "engine/helper.hpp"
#include "PauseMenu.hpp"
#include "ReloadGame.hpp"
#include "LevelEditor.hpp"
#include "CollisionLine.hpp"
#include "Well.hpp"
#include "Kid.hpp"
#include "Beam.hpp"
#include "DeadGhost.hpp"
#include "Orb.hpp"

#include <string>
#include <cmath>
#include <loki/MultiMethods.h>

const std::string Game::backgroundMusic = "../data/sfx/forest.ogg";

Game::Game()
: sharedObject("levels", "Level1"), level(reinterpret_cast<Level*>(sharedObject.loadFunction(
                                        "createLevel")(reinterpret_cast<void*>(this)))),
  levelCoroutine([this](push_type& yield) { level->step(yield); }),
  background(GetPaths().Graphics() + "boden"), foreground(GetPaths().Graphics() + "vig"),
  walls(GetPaths().Graphics() + "wande") {
	GameObject::SetGame(this);

	levelCoroutine(); // Dies sollte die Geister und die Eltern erstellen
	addObjects();
	jngl::setMouseVisible(false);

	jngl::play(backgroundMusic);
}

Game::~Game() {
	jngl::setMouseVisible(true);
	for (auto gameObject : gameObjects_) {
		delete gameObject;
	}
	jngl::stop(backgroundMusic);
}

void Game::step() {
	// Hier und nicht am Ende Objekte hinzufügen, damit diese mindest einmal gesteppt werden,
	// bevor sie gezeichnet werden.
	addObjects();

	levelCoroutine();

	for (auto& gameObject : gameObjects_) {
		gameObject->step();
	}

	CheckCollisionExecutor checkCollsionExec;
	typedef LOKI_TYPELIST_5(const Circle, const Quadrat, const Line, const Point,
	                        const NullShape) CheckCollisionTypes;
	typedef Loki::StaticDispatcher<CheckCollisionExecutor,
	                               const Shape,         // lhs base
	                               CheckCollisionTypes> // lhs types
	    CheckCollisionDispatch;

	{ // Erster Durchlauf mit weniger Objekten, bei dem es darum geht, die Lichtstrahlen zu kürzen
		HandleCollisionExecutor preHandleCollisionExec(*this);
		// Reihenfolge wichtig! Nämlich so wie sie auch von Fire behandelt werden sollen.
		typedef LOKI_TYPELIST_3(Parent, CollisionLine, Beam) preHandleCollisionTypes;
		typedef Loki::StaticDispatcher<HandleCollisionExecutor, GameObject,
		                               preHandleCollisionTypes>
		    preHandleCollisionDispatch;

		const auto end = gameObjects_.end();
		for (auto it = gameObjects_.begin(); it != end; ++it) {
			for (auto it2 = it; it2 != end; ++it2) {
				if (it != it2 && (*it)->getZIndex() < 99999 &&
				    (*it2)->getZIndex() < 99999) { // Geister ausschließen!
					CheckCollisionDispatch::Go((*it)->GetShape(),
					                           (*it2)->GetShape(),
					                           checkCollsionExec);
					if (checkCollsionExec.Collided()) {
						preHandleCollisionExec.SetCollisionPoint(
						    checkCollsionExec.GetCollisionPoint());
						preHandleCollisionDispatch::Go(
						    *(*it), *(*it2), preHandleCollisionExec);
						checkCollsionExec.SetCollided(false);
					}
				}
			}
		}
	}

	HandleCollisionExecutor handleCollisionExec(*this);
	// Reihenfolge wichtig! Nämlich so wie sie auch von Fire behandelt werden sollen.
	typedef LOKI_TYPELIST_10(Ghost, Parent, DeadGhost, Player, ImpassableWall, CollisionLine,
	                         Kid, Well, Beam, Orb) HandleCollisionTypes;
	typedef Loki::StaticDispatcher<HandleCollisionExecutor, GameObject, HandleCollisionTypes>
	    HandleCollisionDispatch;

	const auto end = gameObjects_.end();
	for (auto it = gameObjects_.begin(); it != end; ++it) {
		for (auto it2 = it; it2 != end; ++it2) {
			if (it != it2) {
				CheckCollisionDispatch::Go((*it)->GetShape(), (*it2)->GetShape(),
				                           checkCollsionExec);
				if (checkCollsionExec.Collided()) {
					handleCollisionExec.SetCollisionPoint(
					    checkCollsionExec.GetCollisionPoint());
					HandleCollisionDispatch::Go(*(*it), *(*it2),
					                            handleCollisionExec);
					checkCollsionExec.SetCollided(false);
				}
			}
		}
	}

	if (jngl::keyPressed(jngl::key::F3)) {
		jngl::setWork(std::make_shared<LevelEditor>(shared_from_this(), *level));
	}
	if (jngl::keyPressed(jngl::key::F5)) {
		jngl::setWork(std::make_shared<ReloadGame>());
	}
	if (jngl::keyPressed(jngl::key::Escape)) {
		onQuitEvent();
	}

	// Eventuell soll ein Objekt entfernt werden, welches nicht mehr gezeichnet werden darf.
	// Daher jetzt entfernen:
	removeObjects();

	secondsLeft -= 1.0 / 60.0;

	const auto& kids = GetAll<Kid>();
	size_t kidsSleeping = kids.size();
	for (const auto kid : kids) {
		if (kid->isAwake()) {
			--kidsSleeping;
		}
	}
	if (kidsSleeping == 0) {
		jngl::setWork(std::make_shared<GameOverScreen>(shared_from_this(), true));
	}
	if (secondsLeft <= 0) {
		jngl::setWork(std::make_shared<GameOverScreen>(shared_from_this(), false));
	}
}

void Game::addObjects() {
	for (const auto& gameObject : needToAdd_) {
		gameObjects_.push_back(gameObject);
		addByType<Ghost>(gameObject);
		addByType<Parent>(gameObject);
		addByType<Player>(gameObject);
		addByType<Point>(gameObject);
		addByType<Line>(gameObject);
		addByType<Circle>(gameObject);
		addByType<CollisionLine>(gameObject);
		addByType<Well>(gameObject);
		addByType<Kid>(gameObject);
	}
	needToAdd_.clear();
}

void Game::removeObjects() {
	needToRemove_.sort();
	needToRemove_.unique(); // Remove dublicates

	for (const auto& gameObject : needToRemove_) {
		gameObjects_.remove(gameObject);
		RemoveByType<Ghost>(gameObject); // This list needs to be the same as above
		RemoveByType<Parent>(gameObject);
		RemoveByType<Player>(gameObject);
		RemoveByType<Point>(gameObject);
		RemoveByType<Line>(gameObject);
		RemoveByType<Circle>(gameObject);
		RemoveByType<CollisionLine>(gameObject);
		RemoveByType<Well>(gameObject);
		RemoveByType<Kid>(gameObject);
		delete gameObject;
	}
	needToRemove_.clear();
}

void Game::draw() const {
	background.draw();

	std::multimap<double, GameObject*> orderedByZIndex;
	for (auto obj : gameObjects_) {
		orderedByZIndex.emplace(obj->getZIndex(), obj);
	}

	bool wallsDrawn = false;
	for (const auto& it : orderedByZIndex) {
		if (!wallsDrawn && it.first > 9999) { // Geister haben 99999, daher dieser willkürliche Wert
			walls.draw();
			wallsDrawn = true;
		}
		it.second->draw();
	}
	if (!wallsDrawn) {
		walls.draw();
	}

	foreground.draw();

	std::ostringstream sstream;
	sstream << "Time Left " << int(secondsLeft) / 60 << ":" << std::setfill('0') << std::setw(2)
	        << int(secondsLeft) % 60;
	jngl::setFontColor(255, 255, 255);
	jngl::setFontSize(60);
	jngl::print(sstream.str(), 1555, -1010);

#ifndef NDEBUG
	std::ostringstream tmp;
	tmp << GetScreen().getAbsoluteMousePos().x << " " << GetScreen().getAbsoluteMousePos().y;
	jngl::print(tmp.str(), 0, 0);
	for (auto line : GetAll<CollisionLine>()) {
		line->draw();
	}
#endif
}

void Game::Add(GameObject* const object) {
	needToAdd_.push_back(object);
}

void Game::Remove(GameObject* const object) {
	needToRemove_.push_back(object);
}

void Game::PlayerDies() {
	std::list<Ghost*> playerShips = GetAll<Ghost>();
	if (!playerShips.empty()) {
		Remove(*playerShips.begin());
	}
}

void Game::onQuitEvent() {
	jngl::setWork(std::make_shared<PauseMenu>(jngl::getWork()));
	jngl::cancelQuit();
}

void Game::setNumberOfPlayers(int n) {
	numberOfPlayers = n;
}

int Game::getNumberOfPlayers() const {
	return numberOfPlayers;
}

std::string Game::getLevelTxtPath() const {
	return GetPaths().data() + "levels/" + sharedObject.getName() + ".txt";
}
