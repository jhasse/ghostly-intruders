#pragma once

#include "collisionobjects.hpp"

#include <jngl/sprite.hpp>

class Player;

class Well : public CollisionCircle {
public:
	Well(Vector2d position);
	void step() override;
	void draw() const override;
	void teleport(Player&);

private:
	void receivePlayer(Player&);

	/// Zählt für jeden Player einen countdown runter, damit man nicht sofort wieder zurück
	/// teleportiert wird
	std::map<const Player*, int> recentlyReceived;

	jngl::Sprite sprite;
};
