#include "Player.hpp"

#include "Control.hpp"
#include "engine/coroutines.hpp"
#include "engine/Paths.hpp"
#include "Gamepad.hpp"
#include "Keyboard.hpp"

#include <jngl.hpp>

Player::Player(const size_t playerNr, std::unique_ptr<Control> control) : playerNr(playerNr) {
	if (control) {
		this->control = std::move(control);
	} else {
		const auto controllers = jngl::getConnectedControllers();
		if (controllers.size() > playerNr) {
			this->control.reset(new Gamepad(controllers[playerNr]));
		} else {
			this->control.reset(new Keyboard);
		}
	}
}

Player::~Player() = default;

void Player::stepMovement(const double movementSpeed) {
	if (speed.X() < -0.002) {
		spriteDirection = Direction::LEFT;
	}
	if (speed.X() > 0.002) {
		spriteDirection = Direction::RIGHT;
	}

	acc_ = control->getAcceleration();
	speed += acc_ * movementSpeed;
	speed *= 0.94;
	if (!blockedDirections.empty()) {
		for (const Vector2d& blockedDirection : blockedDirections) {
			const double speedInBlockedDirection = speed * (-1 * blockedDirection);
			if (speedInBlockedDirection > 0) {
				speed += speedInBlockedDirection * blockedDirection;
			}
		}
		blockedDirections.clear();
	}
	position += speed;

	if (animation) {
		try {
			(*animation)();
		} catch (std::exception&) {
			animation = {};
		}
	}
}

void Player::draw() const {
	jngl::setColor(0, 0, 0, 80);
	jngl::drawEllipse(position.x, position.y + 50, 41, 16);
}

double Player::GetRadius() const {
	try {
		return dynamic_cast<const Circle&>(GetShape()).GetRadius();
	} catch (...) {
		return 0;
	}
}

void Player::teleportTo(const Vector2d newPosition) {
	if (animation) { return; }
	jngl::play(GetPaths().data() + "sfx/tele.ogg");
	animation = std::make_unique<pull_type>([this, newPosition](push_type& yield) {
		for (int i = 25; i > 0; --i) {
			animationScale = i / 25.0;
			yield();
		}
		for (int i = 1; i <= 60; ++i) { // Die Zeit im Brunnennetz :P
			position = newPosition;
			yield();
		}
		for (int i = 1; i <= 25; ++i) {
			animationScale = i / 25.0;
			yield();
		}
	});
}

void Player::Accelerate(const Vector2d& acc) {
	speed += acc;
}

void Player::blockMovementFrom(Vector2d direction) {
	blockedDirections.emplace_back(std::move(direction));
}
