#include "Parent.hpp"

#include "Ghost.hpp"
#include "Game.hpp"
#include "Beam.hpp"
#include "engine/Animation.hpp"
#include "engine/Screen.hpp"
#include "engine/Paths.hpp"
#include "Control.hpp"

const unsigned int Parent::RUN_DURATION = 180;
const unsigned int Parent::RUN_COOLDOWN = 240;

Parent::Parent(const Vector2d& position, const int playerNr)
: Player(playerNr), animations{{
	Animation{playerNr < 2 ? "father_stay" : "mother_stay"},
	Animation{playerNr < 2 ? "father_walk_down" : "mother_walk_down"},
}} {
	this->position = position;
	radius_ = 45;
	life_ = 10;
	activeAnimation = &animations[0];

	setShape(new Circle(*this, radius_));
}

void Parent::draw() const {
	Player::draw();
	jngl::pushMatrix();
	jngl::translate(position.X(), position.Y() - 30);
	activeAnimation->draw();
	jngl::popMatrix();
}

void Parent::step() {
	stepMovement(runCountdown > RUN_COOLDOWN ? 0.7 : 0.3);
	activeAnimation->step();
	if (speed.LengthSq() > 2) {
		activeAnimation = &animations[1];
	}
	if (speed.LengthSq() < 0.6) {
		activeAnimation = &animations[0];
	}
	if (control->getDirection().LengthSq() > 0.1) {
		if (!beam) {
			beam = GetGame().add<Beam>(position);
		}
		beam->setPositionAndDirection(position, control->getDirection());
	} else {
		if (beam) {
			GetGame().Remove(beam);
			beam = nullptr;
		}
	}
	if (runCountdown > 0) {
		--runCountdown;
	}
	if (runCountdown > RUN_COOLDOWN && runCountdown % 20 == 0) {
		GetGame()
		    .add<Animation>("dust", 1, false)
		    ->setPosition(position + Vector2d(0, 45));
	}
	if (runCountdown == 0 && control->getShoot()) {
		runCountdown = RUN_DURATION + RUN_COOLDOWN;
		jngl::play(GetPaths().data() + "sfx/sprint.ogg");
	}
}

void Parent::HandleCollisionWithPoint(const Vector2d& point) {
	const Vector2d connection = (position - point).Normalize();
	speed += connection * speed * -(1.8) * connection;
	position = connection * radius_ + point;
}
